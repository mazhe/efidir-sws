/*
 *    Copyright (c) 2008. The EFIDIR team. All right reserved.
 *
 *    This file is part of EFIDIR tools.
 *
 *    EFIDIR tool(s) is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    EFIDIR tool(s) is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public licence
 *    along with EFIDIR tools.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COM_SYSTEM_H
#define COM_SYSTEM_H

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "efidir_assert.h"
#include "efidir_string.h"
#include "efidir_file.h"

int get_core_number();
void log_hwloc_topology(char* xml_file_name);
#endif
