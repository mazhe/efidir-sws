/*****************************************************************************
 *
 * This example source code introduces a c library buffered I/O interface to
 * URL reads it supports fopen(), fread(), fgets(), feof(), fclose(),
 * rewind(). Supported functions have identical prototypes to their normal c
 * lib namesakes and are preceaded by url_ .
 *
 * Using this code you can replace your program's fopen() with url_fopen()
 * and fread() with url_fread() and it become possible to read remote streams
 * instead of (only) local files. Local files (ie those that can be directly
 * fopened) will drop back to using the underlying clib implementations
 *
 * See the main() function at the bottom that shows an app that retrives from a
 * specified url using fgets() and fread() and saves as two output files.
 *
 * Copyright (c) 2003 Simtec Electronics
 *
 * Re-implemented by Vincent Sanders <vince@kyllikki.org> with extensive
 * reference to original curl example code
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This example requires libcurl 7.9.7 or later.
 */ 
 
#include "efidir_file.h"

#ifdef _WIN32
# ifndef O_LARGEFILE
#  define O_LARGEFILE 0x0
# endif
#endif

char *get_next_file_name_from_file(FILE *fd, char *fileName){
  char* get_file_name = NULL;
  size_t sizeOf_get_file_name;
  int c;
  char *path=NULL;

  int nbCharOfImagename = efidir_getline(&get_file_name, &sizeOf_get_file_name, fd);
  c = strlen(get_file_name) -1;

  // remove '\n' at the end of file name
  if (get_file_name[c] == '\n'){
    get_file_name[c] = '\0' ;
  }

  //manage relative or absolute file name
  path = get_file_path(fileName);
  string_cat(path, "/");
  string_cat(path, get_file_name); // the full name is in 'path'
  string_cpy(get_file_name, path); // move the full name in 'imageName'
  efidir_free(path);

  return get_file_name;
}



#ifdef __CURL__
/* we use a global one for convenience */ 
/* CURLM *multi_handle;  */

/* /\* curl calls this routine to get more data *\/  */
/* static size_t */
/* write_callback(char *buffer, */
/*                size_t size, */
/*                size_t nitems, */
/*                void *userp) */
/* { */
/*     char *newbuff; */
/*     int rembuff; */
 
/*     URL_FILE *url = (URL_FILE *)userp; */
/*     size *= nitems; */
 
/*     rembuff=url->buffer_len - url->buffer_pos; /\* remaining space in buffer *\/  */
 
/*     if(size > rembuff) */
/*     { */
/*         /\* not enough space in buffer *\/  */
/*         newbuff=realloc(url->buffer,url->buffer_len + (size - rembuff)); */
/*         if(newbuff==NULL) */
/*         { */
/*             fprintf(stderr,"callback buffer grow failed\n"); */
/*             size=rembuff; */
/*         } */
/*         else */
/*         { */
/*             /\* realloc suceeded increase buffer size*\/  */
/*             url->buffer_len+=size - rembuff; */
/*             url->buffer=newbuff; */
 
/*             /\*printf("Callback buffer grown to %d bytes\n",url->buffer_len);*\/  */
/*         } */
/*     } */
 
/*     memcpy(&url->buffer[url->buffer_pos], buffer, size); */
/*     url->buffer_pos += size; */
 
/*     /\*fprintf(stderr, "callback %d size bytes\n", size);*\/  */
 
/*     return size; */
/* } */
 
/* /\* use to attempt to fill the read buffer up to requested number of bytes *\/  */
/* static int */
/* fill_buffer(URL_FILE *file,int want,int waittime) */
/* { */
/*     fd_set fdread; */
/*     fd_set fdwrite; */
/*     fd_set fdexcep; */
/*     struct timeval timeout; */
/*     int rc; */
 
/*     /\* only attempt to fill buffer if transactions still running and buffer */
/*      * doesnt exceed required size already */
/*      *\/  */
/*     if((!file->still_running) || (file->buffer_pos > want)) */
/*         return 0; */
 
/*     /\* attempt to fill buffer *\/  */
/*     do */
/*     { */
/*         int maxfd = -1; */
/*         FD_ZERO(&fdread); */
/*         FD_ZERO(&fdwrite); */
/*         FD_ZERO(&fdexcep); */
 
/*         /\* set a suitable timeout to fail on *\/  */
/*         timeout.tv_sec = 60; /\* 1 minute *\/  */
/*         timeout.tv_usec = 0; */
 
/*         /\* get file descriptors from the transfers *\/  */
/*         curl_multi_fdset(multi_handle, &fdread, &fdwrite, &fdexcep, &maxfd); */
 
/*         /\* In a real-world program you OF COURSE check the return code of the */
/*            function calls.  On success, the value of maxfd is guaranteed to be */
/*            greater or equal than -1.  We call select(maxfd + 1, ...), specially */
/*            in case of (maxfd == -1), we call select(0, ...), which is basically */
/*            equal to sleep. *\/  */
 
/*         rc = select(maxfd+1, &fdread, &fdwrite, &fdexcep, &timeout); */
 
/*         switch(rc) { */
/*         case -1: */
/*             /\* select error *\/  */
/*             break; */
 
/*         case 0: */
/*             break; */
 
/*         default: */
/*             /\* timeout or readable/writable sockets *\/  */
/*             /\* note we *could* be more efficient and not wait for */
/*              * CURLM_CALL_MULTI_PERFORM to clear here and check it on re-entry */
/*              * but that gets messy *\/  */
/*             while(curl_multi_perform(multi_handle, &file->still_running) == */
/*                   CURLM_CALL_MULTI_PERFORM); */
 
/*             break; */
/*         } */
/*     } while(file->still_running && (file->buffer_pos < want)); */
/*     return 1; */
/* } */
 
/* /\* use to remove want bytes from the front of a files buffer *\/  */
/* static int */
/* use_buffer(URL_FILE *file,int want) */
/* { */
/*     /\* sort out buffer *\/  */
/*     if((file->buffer_pos - want) <=0) */
/*     { */
/*         /\* ditch buffer - write will recreate *\/  */
/*         if(file->buffer) */
/*             free(file->buffer); */
 
/*         file->buffer=NULL; */
/*         file->buffer_pos=0; */
/*         file->buffer_len=0; */
/*     } */
/*     else */
/*     { */
/*         /\* move rest down make it available for later *\/  */
/*         memmove(file->buffer, */
/*                 &file->buffer[want], */
/*                 (file->buffer_pos - want)); */
 
/*         file->buffer_pos -= want; */
/*     } */
/*     return 0; */
/* } */

/* void *__curl_url_open__(const char *url, URL_FILE **file_ptr){ */
/*   URL_FILE *file = (*file_ptr); */
/*   file->type = CFTYPE_CURL; /\* marked as URL *\/  */
/*   file->handle.curl = curl_easy_init(); */
  
/*   curl_easy_setopt(file->handle.curl, CURLOPT_URL, url); */
/*   curl_easy_setopt(file->handle.curl, CURLOPT_WRITEDATA, file); */
/*   curl_easy_setopt(file->handle.curl, CURLOPT_VERBOSE, 0L); */
/*   curl_easy_setopt(file->handle.curl, CURLOPT_WRITEFUNCTION, write_callback); */
  
/*   if(!multi_handle) */
/*     multi_handle = curl_multi_init(); */
  
/*   curl_multi_add_handle(multi_handle, file->handle.curl); */
  
/*   /\* lets start the fetch *\/  */
/*   while(curl_multi_perform(multi_handle, &file->still_running) == */
/*   CURLM_CALL_MULTI_PERFORM ); */
  
/*   /\* if((file->buffer_pos == 0) && (!file->still_running)) *\/ */
/*   /\*   { *\/ */
/*   /\*     /\\* if still_running is 0 now, we should return NULL *\\/  *\/ */
      
/*   /\*     /\\* make sure the easy handle is not in the multi handle anymore *\\/  *\/ */
/*   /\*     curl_multi_remove_handle(multi_handle, file->handle.curl); *\/ */
      
/*   /\*     /\\* cleanup *\\/  *\/ */
/*   /\*     curl_easy_cleanup(file->handle.curl); *\/ */
      
/*   /\*     free(*file_ptr); *\/ */
      
/*   /\*     (*file_ptr) = NULL; *\/ */
/*   /\*   } *\/ */
/* } */
 
/* URL_FILE * */
/* url_open(const char *url, int flags, mode_t mode) */
/* { */
/*     /\* this code could check for URLs or types in the 'url' and */
/*        basicly use the real fopen() for standard files *\/  */
 
/*     URL_FILE *file; */
 
/*     file = malloc(sizeof(URL_FILE)); */
/*     if(!file) */
/*         return NULL; */
 
/*     memset(file, 0, sizeof(URL_FILE)); */
 
/*     if((file->handle.fd=OPEN(url, flags, mode))!=-1) */
/*     { */
/*         file->type = CFTYPE_FILE; /\* marked as LOCAL FILE *\/  */
/*   file->open_mthod = OTYPE_OPEN; */
/*     } */
/*     else */
/*     { */
/*       __curl_url_open__(url, &file); */
/*     } */
/*     return file; */
/* } */
 
/* URL_FILE * */
/* url_fopen(const char *url,const char *operation) */
/* { */
/*     /\* this code could check for URLs or types in the 'url' and */
/*        basicly use the real fopen() for standard files *\/  */
 
/*     URL_FILE *file; */
/*     (void)operation; */
 
/*     file = malloc(sizeof(URL_FILE)); */
/*     if(!file) */
/*         return NULL; */
 
/*     memset(file, 0, sizeof(URL_FILE)); */

/*     if((file->handle.file=fopen(url,operation))) */
/*     { */
/*         file->type = CFTYPE_FILE; /\* marked as LOCAL FILE *\/  */
/*   file->open_mthod = OTYPE_FOPEN; */
/*     } */
/*     else */
/*     { */
/*       __curl_url_open__(url, &file); */
/*     } */
/*     return file; */
/* } */

/* int */
/* url_close(URL_FILE *file){ */
/*   url_fclose(file); */
/* } */
/* int */
/* url_fclose(URL_FILE *file) */
/* { */
/*     int ret=0;/\* default is good return *\/  */
 
/*     switch(file->type) */
/*     { */
/*     case CFTYPE_FILE: */
/*       if(file->open_mthod == OTYPE_FOPEN) */
/*         ret=fclose(file->handle.file); /\* passthrough *\/  */
/*       else if(file->open_mthod == OTYPE_OPEN) */
/*   ret=close(file->handle.fd); /\* passthrough *\/ */
/*       break; */
 
/*     case CFTYPE_CURL: */
/*         /\* make sure the easy handle is not in the multi handle anymore *\/  */
/*         curl_multi_remove_handle(multi_handle, file->handle.curl); */
 
/*         /\* cleanup *\/  */
/*         curl_easy_cleanup(file->handle.curl); */
/*         break; */
 
/*     default: /\* unknown or supported type - oh dear *\/  */
/*         ret=EOF; */
/*         errno=EBADF; */
/*         break; */
 
/*     } */
 
/*     if(file->buffer) */
/*         free(file->buffer);/\* free any allocated buffer space *\/  */
 
/*     free(file); */
 
/*     return ret; */
/* } */
 
/* int */
/* url_feof(URL_FILE *file) */
/* { */
/*     int ret=0; */
 
/*     switch(file->type) */
/*     { */
/*     case CFTYPE_FILE: */
/*         ret=feof(file->handle.file); */
/*         break; */
 
/*     case CFTYPE_CURL: */
/*         if((file->buffer_pos == 0) && (!file->still_running)) */
/*             ret = 1; */
/*         break; */
/*     default: /\* unknown or supported type - oh dear *\/  */
/*         ret=-1; */
/*         errno=EBADF; */
/*         break; */
/*     } */
/*     return ret; */
/* } */
/* ssize_t  */
/* url_read(URL_FILE *file, void *buf, size_t count) */
/* { */
/*     ssize_t want; */
 
/*     switch(file->type) */
/*     { */
/*     case CFTYPE_FILE: */
/*         want=read(file->handle.fd, buf, count); */
/*         break; */
 
/*     case CFTYPE_CURL: */
/*         want = count; */
 
/*         fill_buffer(file,want,1); */
 
/*         /\* check if theres data in the buffer - if not fill_buffer() */
/*          * either errored or EOF *\/  */
/*         if(!file->buffer_pos) */
/*             return 0; */
 
/*         /\* ensure only available data is considered *\/  */
/*         if(file->buffer_pos < want) */
/*             want = file->buffer_pos; */
 
/*         /\* xfer data to caller *\/  */
/*         memcpy(buf, file->buffer, want); */
 
/*         use_buffer(file,want); */
 
/*         /\*printf("(fread) return %d bytes %d left\n", want,file->buffer_pos);*\/  */
/*         break; */
 
/*     default: /\* unknown or supported type - oh dear *\/  */
/*         want=0; */
/*         errno=EBADF; */
/*         break; */
 
/*     } */
/*     return want; */
/* }  */
/* size_t */
/* url_fread(void *ptr, size_t size, size_t nmemb, URL_FILE *file) */
/* { */
/*     size_t want; */
 
/*     switch(file->type) */
/*     { */
/*     case CFTYPE_FILE: */
/*         want=fread(ptr,size,nmemb,file->handle.file); */
/*         break; */
 
/*     case CFTYPE_CURL: */
/*       want = url_read(file, ptr, (size_t)(nmemb * size)); */
/*       want = want / size;     /\* number of items - nb correct op - checked */
/*              * with glibc code*\/  */
/*     } */
/*     return want; */
/* } */
 
/* char * */
/* url_fgets(char *ptr, int size, URL_FILE *file) */
/* { */
/*     int want = size - 1;/\* always need to leave room for zero termination *\/  */
/*     int loop; */
 
/*     switch(file->type) */
/*     { */
/*     case CFTYPE_FILE: */
/*         ptr = fgets(ptr,size,file->handle.file); */
/*         break; */
 
/*     case CFTYPE_CURL: */
/*         fill_buffer(file,want,1); */
 
/*         /\* check if theres data in the buffer - if not fill either errored or */
/*          * EOF *\/  */
/*         if(!file->buffer_pos) */
/*             return NULL; */
 
/*         /\* ensure only available data is considered *\/  */
/*         if(file->buffer_pos < want) */
/*             want = file->buffer_pos; */
 
/*         /\*buffer contains data *\/  */
/*         /\* look for newline or eof *\/  */
/*         for(loop=0;loop < want;loop++) */
/*         { */
/*             if(file->buffer[loop] == '\n') */
/*             { */
/*                 want=loop+1;/\* include newline *\/  */
/*                 break; */
/*             } */
/*         } */
 
/*         /\* xfer data to caller *\/  */
/*         memcpy(ptr, file->buffer, want); */
/*         ptr[want]=0;/\* allways null terminate *\/  */
 
/*         use_buffer(file,want); */
 
/*         /\*printf("(fgets) return %d bytes %d left\n", want,file->buffer_pos);*\/  */
/*         break; */
 
/*     default: /\* unknown or supported type - oh dear *\/  */
/*         ptr=NULL; */
/*         errno=EBADF; */
/*         break; */
/*     } */
 
/*     return ptr;/\*success *\/  */
/* } */
 
/* void */
/* url_rewind(URL_FILE *file) */
/* { */
/*     switch(file->type) */
/*     { */
/*     case CFTYPE_FILE: */
/*         rewind(file->handle.file); /\* passthrough *\/  */
/*         break; */
 
/*     case CFTYPE_CURL: */
/*         /\* halt transaction *\/  */
/*         curl_multi_remove_handle(multi_handle, file->handle.curl); */
 
/*         /\* restart *\/  */
/*         curl_multi_add_handle(multi_handle, file->handle.curl); */
 
/*         /\* ditch buffer - write will recreate - resets stream pos*\/  */
/*         if(file->buffer) */
/*             free(file->buffer); */
 
/*         file->buffer=NULL; */
/*         file->buffer_pos=0; */
/*         file->buffer_len=0; */
 
/*         break; */
 
/*     default: /\* unknown or supported type - oh dear *\/  */
/*         break; */
 
/*     } */
 
/* } */
 
 
/* /\* Small main program to retrive from a url using fgets and fread saving the */
/*  * output to two test files (note the fgets method will corrupt binary files if */
/*  * they contain 0 chars *\/  */
/* char* */
/* url_download1(const char *url) */
/* { */
/*     URL_FILE *handle; */
/*     FILE *outf; */
 
/*     int nread; */
/*     char buffer[256]; */

/*     char *filename  = get_file_name((char*)url); */
/*     string_cat(filename, get_file_extension((char*)url)); */
 
/*     /\* Copy from url with fread *\/  */
/*     outf=fopen(filename,"w+"); */
/*     if(!outf) */
/*     { */
/*         perror("couldn't open fread output file\n"); */
/*         return NULL; */
/*     } */
 
/*     handle = url_fopen(url, "r"); */
/*     if(!handle) { */
/*       fprintf(stderr,"couldn't url_fopen() %s\n", url); */
/*       fclose(outf); */
/*       return NULL; */
/*     } */
 
/*     do { */
/*         nread = url_fread(buffer, 1,sizeof(buffer), handle); */
/*         fwrite(buffer,1,nread,outf); */
/*     } while(nread); */
 
/*     url_fclose(handle); */
 
/*     fclose(outf); */
 
/*     return filename;/\* all done *\/  */
/* } */

 
/*
 * This is an example showing how to get a single file from an FTP server.
 * It delays the actual destination file creation until the first write
 * callback so that it won't create an empty file in case the remote file
 * doesn't exist or something else fails.
 */ 
 
static size_t __save_downloaded_file(void *buffer, size_t size, size_t nmemb, void *stream)
{
  fileInfo *out=(fileInfo *)stream;
  if(out && !out->stream) {
    /* open file for writing */ 
    out->stream=fopen(out->filename, "wb");
    if(!out->stream)
      return -1; /* failure, can't open file to write */ 
  }
  return fwrite(buffer, size, nmemb, out->stream);
}
 
 
char *url_download(const char *remoteFileName){

  if(strstr(remoteFileName, "://") != NULL){

    CURL *curl;
    CURLcode res;
    fileInfo fileinfo;
    fileinfo.filename = new_string("EFIDIRTools_download_");
    string_cat(fileinfo.filename, get_file_name((char*)remoteFileName));
    string_cat(fileinfo.filename, get_file_extension((char*)remoteFileName));
    fileinfo.stream = NULL;
    
    
    curl_global_init(CURL_GLOBAL_DEFAULT);
    
    curl = curl_easy_init();
    if(curl) {
      /*
       * Get curl 7.9.2 from sunet.se's FTP site. curl 7.9.2 is most likely not
       * present there by the time you read this, so you'd better replace the
       * URL with one that works!
       */ 
      curl_easy_setopt(curl, CURLOPT_URL, remoteFileName);
      /* Define our callback to get called when there's data to be written */ 
      curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, __save_downloaded_file);
      /* Set a pointer to our struct to pass to the callback */ 
      curl_easy_setopt(curl, CURLOPT_WRITEDATA, &fileinfo);
      
      /* Switch on full protocol/debug output */ 
      curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
      
      fprintf(stdout,"%s download started\n", remoteFileName);
      res = curl_easy_perform(curl);
      
      /* always cleanup */ 
      curl_easy_cleanup(curl);
      
      if(CURLE_OK != res) {
  /* we failed */ 
  fprintf(stderr, "%s download failes %d\n", remoteFileName, res);
  fileinfo.filename = NULL;
      }
    }
 
    if(fileinfo.stream)
      fclose(fileinfo.stream); /* close the local file */ 
    
    curl_global_cleanup();
    return strdup(fileinfo.filename);
  }

  return NULL;
}

#endif //__CURL__

int efidir_open_no_check(const char *url, int flags, mode_t mode){
  int fd;
#ifndef _WIN32
 fd = open(url, flags, mode);
#else
 fd = open(url, flags, mode|O_BINARY|O_LARGEFILE);
#endif 
#ifdef __CURL__
  char *local_name;
  if(fd==-1){
    local_name = url_download(url);
    if(local_name != NULL){
# ifndef _WIN32
      fd = open(local_name, flags, mode);
# else
#  ifdef _WIN64
      fd = open(url, flags, mode|O_BINARY);
#  else 
      fd = open(url, flags, mode|O_BINARY|O_LARGEFILE);
#  endif
# endif
    }
  }
#endif //__CURL__
  //efidir_assert_fmessage(fd>=0, "Cannot open %s\n", url);
  return fd;
}
int efidir_open(const char *url, int flags, mode_t mode){
  int fd = efidir_open_no_check(url, flags, mode);
  
  efidir_assert_fmessage(fd>=0, "Cannot open %s\n", url);

  return fd;

}

FILE* efidir_fopen_no_check(const char *url,const char *operation){
  FILE *file=fopen(url, operation);
#ifdef __CURL__
  char *local_name;
  if(file==NULL){
    local_name = url_download(url);
    if(local_name != NULL){
      file =  fopen(local_name, operation);
    }
  }
#else //__CURL__ __NO__CURL__
  if(strstr(url, "://") != NULL){
    fprintf(stderr, "\nWARNING: Compilation with lib_CURL required to access to %s\n\n",url);
  }
#endif //__NO__CURL__
  //efidir_assert_fmessage(file!=NULL, "Cannot open %s\n", url);
  return file;
}

FILE* efidir_fopen(const char *url,const char *operation){
  FILE *file=efidir_fopen_no_check(url, operation);

  efidir_assert_fmessage(file!=NULL, "Cannot open %s\n", url);

  return file;
}

int efidir_close(int fd){
    return close(fd);
}
int efidir_fclose(FILE *fp){
  if(fp != NULL){
    return fclose(fp);
  }
  return 0;
}

int efidir_lseek(int fd, int64_t offset, int whence) {
#ifndef _WIN32
  return lseek(fd, offset, whence);
#else
  return _lseeki64(fd, offset, whence);
#endif
}

int exists_localy(const char *fname)
{
    FILE *file;
    file = fopen(fname, "r");
	if (file == NULL) {
        fclose(file);
        return 1;
    }
    return 0;
}
