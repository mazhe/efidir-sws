/* 
 *    Copyright (c) 2008. The EFIDIR team. All right reserved. 
 * 
 *    This file is part of EFIDIR tools. 
 * 
 *    EFIDIR tool(s) is free software: you can redistribute it and/or modify 
 *    it under the terms of the GNU General Public License as published by 
 *    the Free Software Foundation, either version 3 of the License, or 
 *    (at your option) any later version. 
 * 
 *    EFIDIR tool(s) is distributed in the hope that it will be useful, 
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 *    GNU General Public License for more details. 
 * 
 *    You should have received a copy of the GNU General Public licence 
 *    along with EFIDIR tools.  If not, see <http://www.gnu.org/licenses/>. 
 */
/*
 * author: Matthieu Volat / ISTerre
 */

#include <Python.h>

#include <efidir_sws.h>

#include "capsulethunk.h"

static PyObject *
_set_njobs(PyObject *self, PyObject *args)
{
	int njobs;

	(void)self;

    if (!PyArg_ParseTuple(args, "i", &njobs)) {
        return NULL;
    }

    return Py_BuildValue("i", set_njobs(njobs));
}

static PyObject *
_get_njobs(PyObject *self, PyObject *args)
{
	(void)self;

    if (!PyArg_ParseTuple(args, "")) {
        return NULL;
    }

    return Py_BuildValue("i", get_njobs());
}

static PyObject *
_target_create(PyObject *self, PyObject *args)
{
	PyObject *commands, *inputs, *outputs;
	Target t;

	(void)self;

    int i;

    if (!PyArg_ParseTuple(args, "OOO", &commands, &inputs, &outputs)) {
        return NULL;
    }

	t = target_create();
    for (i = 0; i < PyList_Size(commands); i++) {
#if PY_MAJOR_VERSION >= 3
        PyObject *cmd_i = PyList_GetItem(commands, i);
        PyObject *tmp = PyUnicode_AsEncodedString(cmd_i, "ASCII", "strict");
        target_addcommand(t, PyBytes_AsString(tmp));
        Py_DECREF(tmp);
#else
        target_addcommand(t, PyString_AsString(PyList_GetItem(commands, i)));
#endif
    }
    for (i = 0; i < PyList_Size(inputs); i++) {
#if PY_MAJOR_VERSION >= 3
        PyObject *in_i = PyList_GetItem(inputs, i);
        PyObject *tmp = PyUnicode_AsEncodedString(in_i, "ASCII", "strict");
        target_addinput(t, PyBytes_AsString(tmp));
        Py_DECREF(tmp);
#else
        target_addinput(t, PyString_AsString(PyList_GetItem(inputs, i)));
#endif
    }
    for (i = 0; i < PyList_Size(outputs); i++) {
#if PY_MAJOR_VERSION >= 3
        PyObject *out_i = PyList_GetItem(outputs, i);
        PyObject *tmp = PyUnicode_AsEncodedString(out_i, "ASCII", "strict");
        target_addoutput(t, PyBytes_AsString(tmp));
        Py_DECREF(tmp);
#else
        target_addoutput(t, PyString_AsString(PyList_GetItem(outputs, i)));
#endif
    }

    return PyCapsule_New(t, "Target", NULL);
}

static PyObject *
_workflow_create(PyObject *self, PyObject *args)
{
	PyObject *targets;
	char *name;
	Workflow c;

	int i;

	(void)self;

    if (!PyArg_ParseTuple(args, "zO", &name, &targets)) {
        return NULL;
    }

	c = workflow_create(name);
	for (i = 0; i < PyList_Size(targets); i++) {
		Target t;
		t = PyCapsule_GetPointer(PyList_GetItem(targets, i), "Target");
		workflow_addtarget(c, t);
	}

    return PyCapsule_New(c, "Workflow", NULL);
}

static PyObject *
_workflow_save(PyObject *self, PyObject *args)
{
	PyObject *wf;
	char *path;

	(void)self;

    if (!PyArg_ParseTuple(args, "Oz", &wf, &path)) {
        return NULL;
    }
	
	workflow_save((Workflow)PyCapsule_GetPointer(wf, "Workflow"), path);

	Py_INCREF(Py_None);
	return Py_None;
}

static PyObject *
_workflow_run(PyObject *self, PyObject *args)
{
	PyObject *wf, *coe;

	(void)self;

	switch(PyTuple_Size(args)) {
	case 1:
		if (!PyArg_ParseTuple(args, "O", &wf)) {
			return NULL;
		}
        coe = Py_False;
		break;
	case 2:
		if (!PyArg_ParseTuple(args, "OO", &wf, &coe)) {
			return NULL;
		}
		break;
	default:
		return NULL;
	}
	
	workflow_run((Workflow)PyCapsule_GetPointer(wf, "Workflow"), coe == Py_True);

	Py_INCREF(Py_None);
	return Py_None;
}

static PyObject *
_workflow_dryrun(PyObject *self, PyObject *args)
{
	PyObject *wf, *coe;

	(void)self;

	switch(PyTuple_Size(args)) {
	case 1:
		if (!PyArg_ParseTuple(args, "O", &wf)) {
			return NULL;
		}
        coe = Py_False;
		break;
	case 2:
		if (!PyArg_ParseTuple(args, "OO", &wf, &coe)) {
			return NULL;
		}
		break;
	default:
		return NULL;
	}
	
	workflow_dryrun((Workflow)PyCapsule_GetPointer(wf, "Workflow"), coe == Py_True);

	Py_INCREF(Py_None);
	return Py_None;
}

struct module_state {
    PyObject *error;
};

#if PY_MAJOR_VERSION >= 3
#define GETSTATE(m) ((struct module_state*)PyModule_GetState(m))
#else
#define GETSTATE(m) (&_state)
static struct module_state _state;
#endif

static PyObject *
error_out(PyObject *m) {
    struct module_state *st = GETSTATE(m);
    PyErr_SetString(st->error, "something bad happened");
    return NULL;
}

static PyMethodDef sws_methods[] = {
    {"error_out", (PyCFunction)error_out, METH_NOARGS, NULL},
    {"set_njobs", _set_njobs, METH_VARARGS,
        ""},
    {"get_njobs", _get_njobs, METH_VARARGS,
        ""},
    {"target_create", _target_create, METH_VARARGS,
        ""},
    {"workflow_create", _workflow_create, METH_VARARGS,
        ""},
    {"workflow_save", _workflow_save, METH_VARARGS,
        ""},
    {"workflow_run", _workflow_run, METH_VARARGS,
        ""},
    {"workflow_dryrun", _workflow_dryrun, METH_VARARGS,
        ""},
	/* Deprecated function names */
    {"chain_create", _workflow_create, METH_VARARGS,
        ""},
    {"chain_run", _workflow_run, METH_VARARGS,
        ""},
    {"chain_save", _workflow_save, METH_VARARGS,
        ""},
    /* */
    {NULL, NULL}
};

#if PY_MAJOR_VERSION >= 3

static int sws_traverse(PyObject *m, visitproc visit, void *arg) {
    Py_VISIT(GETSTATE(m)->error);
    return 0;
}

static int sws_clear(PyObject *m) {
    Py_CLEAR(GETSTATE(m)->error);
    return 0;
}


static struct PyModuleDef moduledef = {
        PyModuleDef_HEAD_INIT,
        "sws",
        NULL,
        sizeof(struct module_state),
        sws_methods,
        NULL,
        sws_traverse,
        sws_clear,
        NULL
};

#define INITERROR return NULL

PyMODINIT_FUNC
PyInit_sws(void)

#else
#define INITERROR return

void
initsws(void)
#endif
{
#if PY_MAJOR_VERSION >= 3
    PyObject *module = PyModule_Create(&moduledef);
#else
    PyObject *module = Py_InitModule("sws", sws_methods);
#endif

    if (module == NULL)
        INITERROR;
    struct module_state *st = GETSTATE(module);

    st->error = PyErr_NewException("sws.Error", NULL, NULL);
    if (st->error == NULL) {
        Py_DECREF(module);
        INITERROR;
    }

#if PY_MAJOR_VERSION >= 3
    return module;
#endif
}
