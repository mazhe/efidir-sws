/*
 *    Copyright (c) 2008. The EFIDIR team. All right reserved.
 *
 *    This file is part of EFIDIR tools.
 *
 *    EFIDIR tool(s) is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    EFIDIR tool(s) is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public licence
 *    along with EFIDIR tools.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * \ingroup goodies
 * \defgroup assertion assertion functions
 * \file efidir_assert.h
 * \author Flavien Vernier
 * 
 * \brief Definition of efidir assertion functions.
 * 
 * Definition of efidir assertions functions that point the file and line error.
 * 
 */

#ifndef EFIDIR_ASSERT_H
#define EFIDIR_ASSERT_H

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#ifdef NDEBUG
#define efidir_assert_debug(ignore) ((void) 0)
#else
/**
 * \ingroup assertion
 * \def efidir_assert_debug
 * \brief Debuging asertion
 *
 * \param expression Boolean - expression.
 *
 * Stop the execution if the 'expression' is false and 
 * display on the error output the source file name and 
 * the line number where the assertion is failed. 
 * The debuging asertion are automatically removed 
 * if the code is not compiled with debuging mode
 *
 */
#define efidir_assert_debug(expression)  \
  (__efidir_assert (expression, __FILE__, __LINE__))
#endif /* NDEBUG */

/**
 * \ingroup assertion
 * \def efidir_assert
 * \brief Assertion.
 *
 * \param expression Boolean - expression.
 *
 * Stop the execution if the 'expression' is false and 
 * display on the error output the source file name and 
 * the line number where the assertion is failed. 
 */
#define efidir_assert(expression)  \
  ( __efidir_assert ((expression), __FILE__, __LINE__))

#define __efidir_assert(expression, file, lineno)  \
  ((void) ((expression) ? 0 : (fprintf (stderr, "%s:%u: assertion failed: %d\n", file, lineno, expression), \
   exit(EXIT_FAILURE), 0)))

/**
 * \ingroup assertion
 * \def efidir_assert_message
 * \brief Assertion with message.
 *
 * \param expression Boolean - expression.
 * \param message const char* - information message.
 *
 * Stop the execution if the 'expression' is false and 
 * display on the error output the source file name, 
 * the line number where the assertion is failed and 
 * the given 'message'
 */
#define efidir_assert_message(expression, message) \
  ( __efidir_assert_message ((expression), message, __FILE__, __LINE__))

#define __efidir_assert_message(expression, message, file, lineno)  \
  ((void) ((expression) ? 0 : (fprintf (stderr, "%s:%u: assertion failed: %s\n", file, lineno, message), \
   exit(EXIT_FAILURE), 0)))

/**
 * \ingroup assertion
 * \def efidir_assert_fmessage
 * \brief Assertion with formated message.
 *
 * \param expression Boolean - expression.
 * \param format const char* - message format.
 * \param ... .
 *
 * Stop the execution if the 'expression' is false and 
 * display on the error output the source file name, 
 * the line number where the assertion is failed and 
 * the given 'message' according to 'format'.
 */
#define efidir_assert_fmessage(expression, format, ...) \
  ( __efidir_assert_fmessage ((expression), __FILE__, __LINE__, format, __VA_ARGS__))

#define __efidir_assert_fmessage(expression, file, lineno, format, ...)  \
  ((void) ((expression) ? 0 : ( \
   fprintf (stderr, "%s:%u: assertion failed : ", file, lineno),	\
   fprintf(stderr, format, __VA_ARGS__), fprintf(stderr, "\n"), exit(EXIT_FAILURE), 0)))

#endif /* EFIDIR_ASSERT_H */
