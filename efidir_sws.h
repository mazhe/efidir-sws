/*    
 *    Copyright (c) 2013. The EFIDIR team. All right reserved.
 *
 *    This file is part of EFIDIR tools.
 *
 *    EFIDIR tool(s) is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    EFIDIR tool(s) is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with EFIDIR tools.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \ingroup efidir 
 * \defgroup sws Scientific Workflow System
 * \author Matthieu Volat (ISTerre)
 * 
 * \brief EFIDIR scientific workflow system.
 */

#ifndef EFIDIR_SWS
#define EFIDIR_SWS

#include <stdbool.h>
#include <stddef.h>

typedef enum enum_target_properties {
	TARGET_PHONY		= 0x1,
} target_properties;

/**
 * \ingroup sws
 * \typedef Target
 * \brief Action to be executed in a workflow context.
 */
typedef struct struct_target target, *Target;

/**
 * \ingroup sws
 * \typedef Workflow
 * \brief A workflow.
 */
typedef struct struct_workflow workflow, *Workflow;

/**
 * \ingroup sws
 * \brief Set the number of parallel jobs for the workflows.
 *
 * Usually, the SWS module will attempt to guess the capacity of the 
 * runtime to execute tasks. This function allows to override this guess
 * and manually set a fixed maximum of parallelism in the module.
 *
 * This setting will apply to all the workflow run in the current
 * session.
 *
 * \param njobs  Maximum number of parallel jobs
 *
 * \return New maximum
 */
size_t set_njobs(size_t njobs);

/**
 * \ingroup sws
 * \brief Get the number of parallel jobs for the workflows.
 *
 * Return the maximum parallel tasks allowed by the workflow module.
 *
 * \return Maximum of parallel tasks
 */
size_t get_njobs(void);

/**
 * \ingroup sws
 * \brief Create a new target.
 *
 * Create a new, empty, target. The target must then be populated 
 * with commands, inputs, outputs using the target_addcommand(),
 * target_addworkflow(), target_addinput(), target_addoutput() 
 * functions.
 *
 * \return Reference to the target
 */
Target target_create();
/**
 * \ingroup sws
 * \brief Destroy a target.
 *
 * Destroy/free a workflow object. Note that this can cause issue if
 * a workflow where this target is used is being executed, and may result
 * in either a critical  error or won't stop the workflow.
 *
 * \param target  Reference to the target
 */
void target_destroy(Target target);
/**
 * \ingroup sws
 * \brief Add a single command to a target
 *
 * This function allows you to add a single command to a target. A 
 * command is expressed as a single line shell expression.
 *
 * Adding multiple commands will result in said commands be executed
 * sequentially as part of the target.
 *
 * If the command is actually a workflow, use target_addcommand() 
 * instead of target_addcommand().
 *
 * \param target   Reference to the target
 * \param command  String storing the command
 */
void target_addcommand(Target target, const char *command);
/**
 * \ingroup sws
 * \brief Add a single workflow to a target
 *
 * This function works as target_addcommand(), except its usage
 * specify that the command is actually a workflow itself. This 
 * will alter the way the command is run to adapt to this 
 * specific context.
 *
 * \param target   Reference to the target
 * \param command  Workflow command string
 */
void target_addworkflow(Target target, const char *command);
/**
 * \ingroup sws
 * \brief Add an input to a target
 *
 * This function specify that a target depend on a specific input
 * file.
 *
 * For the SWS module to work as expected, inputs and outputs must
 * be declared as best as possible.
 *
 * \param target   Reference to the target
 * \param input    Path to the input
 */
void target_addinput(Target target, const char *input);
/**
 * \ingroup sws
 * \brief Add an output to a target
 *
 * This function specify that a target produce a specific output
 * file.
 *
 * For the SWS module to work as expected, inputs and outputs must
 * be declared as best as possible.
 *
 * \param target   Reference to the target
 * \param input    Path to the output
 */
void target_addoutput(Target target, const char *output);
/**
 * \ingroup sws
 * \brief Set target property
 *
 * This function can be used to set specific options on a target.
 *
 * \param target   Reference to the target
 * \param properties Properties to apply
 */
void target_setproperties(Target target, target_properties properties);

/**
 * \ingroup sws
 * \brief Create a new workflow.
 *
 * Create a new, empty, workflow. The workflow must then be populated 
 * with targets using the chain_addtarget() function.
 *
 * \return Reference to the workflow
 */
Workflow workflow_create(const char *name);
/**
 * \ingroup sws
 * \brief Destroy a workflow.
 *
 * Destroy/free a workflow object, including its internal targets.
 *
 * Note that this can cause issue if the workflow is being executed.
 *
 * This function is intended to be called outside of a execution to
 * cleanup the context.
 *
 * \param workflow Reference to the workflow to destroy
 */
void workflow_destroy(Workflow wf);
/**
 * \ingroup sws
 * \brief Add an existing target to a workflow
 *
 * Workflows are created empty. workflow_addtarget() is used to add
 * tasks to them. Dependencies between tasks are resolved based on
 * the inputs/outputs of the tasks.
 *
 * A workflow will try to generate the outputs of all its tasks.
 *
 * \param workflow  Reference to the workflow
 * \param target    Reference to the task to add
 */
void workflow_addtarget(Workflow wf, Target target);
/**
 * \ingroup sws
 * \brief Save the Makefile generated for a workflow
 *
 * Save the workflow Makefile. This is often done for debugging
 * purposes. Makefile generated with the MPI backend require to
 * have a running efidir_dispatchd instance to actually execute
 * commands.
 *
 * \param workflow  Reference to the workflow
 * \param path      Path to the output file
 */
void workflow_save(Workflow wf, const char *path);
/**
 * \ingroup sws
 * \brief Execute a workflow
 *
 * Execute the workflow. This function will block until the workflow
 * has been completely executed.
 *
 * \param workflow           Reference to the workflow
 * \param continue_on_error  If an error happens in the processing, but
 *                           some part of it can still be realized,
 *                           keep processing those parts
 */
void workflow_run(Workflow wf, bool continue_on_error);
/**
 * \ingroup sws
 * \brief Simulate a workflow
 *
 * Pseudo-run a workflow: commands that would be executed are printed,
 * but not really executed. This function will block until the simulation
 * has been terminated.
 *
 * \param workflow            Reference to the workflow
 * \param continue_no_inputs  Continue the dryrun if the initial inputs are
 *                            not found
 */
void workflow_dryrun(Workflow wf, bool continue_no_inputs);

#endif /* ndef EFIDIR_SWS */
