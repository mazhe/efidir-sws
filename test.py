#!/usr/bin/env python2
from sws import *
set_njobs(1)
tasks = []
# Create some text file
hello1_ins = []
hello1_outs = ["hello1"]
hello1_cmds = ["echo foo > "+hello1_outs[0] ]
hello1_task = target_create(hello1_cmds, hello1_ins, hello1_outs)
tasks.append(hello1_task) # Append to task list
# Then, export
hello2_outs = ["hello2"]
hello2_cmds = ["sed 's/foo/faa/' "+hello1_outs[0]+" > "+hello2_outs[0]]
hello2_task = target_create(hello2_cmds, hello1_outs, hello2_outs)
tasks.append(hello2_task) # Append to task list
# Another target, do not depend on either hello1 & 2
hello3_ins = []
hello3_outs = ["hello3"]
hello3_cmds = ["echo bar > "+hello3_outs[0]]
hello3_task = target_create(hello3_cmds, hello3_ins, hello3_outs)
tasks.append(hello3_task) # Append to task list
# Finaly, create and run workflow
w = workflow_create("do_the_stuff", tasks)
workflow_run(w)
