/* Copyright (c) 2008. The EFIDIR team. All right reserved.
 *
 * This file is part of EFIDIR tools.
 *
 * EFIDIR tool(s) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EFIDIR tool(s) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EFIDIR tools.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file efidir_boolean.h
 * \author Flavien Vernier
 * 
 * \brief Boolean type definition.
 * 
 * Just a definition of Boolean type and values FALSE (or false) and TRUE (or true).
 *
 * Example: "Boolean var_name = true;"
 *
 */
#ifndef _BOOLEAN_H
#define _BOOLEAN_H

#ifndef HAVE_BOOLEAN
#ifndef bool
#ifndef __cplusplus
#define HAVE_BOOLEAN 1
//#define boolean int
typedef int bool;
#endif
#endif
#endif

#ifndef false
#define false 0
#endif

#ifndef true
#define true 1
#endif

#endif /* _BOOLEAN_H */
