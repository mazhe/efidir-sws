/*    
 *    Copyright (c) 2013. The EFIDIR team. All right reserved.
 *
 *    This file is part of EFIDIR tools.
 *
 *    EFIDIR tool(s) is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    EFIDIR tool(s) is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with EFIDIR tools.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * author: Matthieu Volat / ISTerre
 * 
 * EFIDIR stdint compat header
 */


#ifndef EFIDIR_STDINT
#define EFIDIR_STDINT

#ifdef _WIN32
typedef __int64  int64_t;
typedef unsigned __int64 uint64_t;
#else
# include <stdint.h>
#endif

#endif /* ndef EFIDIR_STDINT */
