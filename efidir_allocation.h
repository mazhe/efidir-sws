/* Copyright (c) 2008. The EFIDIR team. All right reserved.
 *
 * This file is part of EFIDIR tools.
 *
 * EFIDIR tool(s) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EFIDIR tool(s) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EFIDIR tools.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \ingroup goodies
 * \defgroup allocation memory allocations
 * \file efidir_allocation.h
 * \author Flavien Vernier (LISTIC)
 * 
 * \brief Re-definition of malloc calloc and realloc.
 * 
 * Re-definition of malloc calloc and realloc with an error management.
 * 
 */

#ifndef EFIDIR_ALLOCATION_H
#define EFIDIR_ALLOCATION_H

#include<stdlib.h>
#include<stdio.h>

#include "efidir_assert.h"

/**
 * \ingroup allocation
 * \def efidir_malloc(int n)
 * \brief Like malloc, but die on error 
 *
 * \param n int - size allocated in bytes.
 * \return the value returned is a pointer to the allocated memory.
 *
 */
#define efidir_malloc(n)(__efidir_malloc(n, __FILE__, __LINE__))
static inline void *__efidir_malloc(int n, char *source_file, int source_line){
  void *res=NULL;
  //__efidir_assert_message((n>0), "cannot alloc size 0 ", source_file, source_line);
  if(n<=0){
    fprintf(stderr,"WARNING: alloc size 0 in %s:%i",source_file, source_line);
  }else{
    res=malloc(n);
    __efidir_assert_message(res!=NULL, "alocation failed", source_file, source_line);
  }
  return res;
  }

/** 
 * \ingroup allocation
 * \def efidir_calloc(int n, int s)
 * \brief Like calloc, but die on error and memset data to 0
 *
 * \param n int - number of elements
 * \param s int - size of an element
 * \return the value returned is a pointer to the allocated memory.
 */
#define efidir_calloc(n, s)(__efidir_calloc(n, s, __FILE__, __LINE__))
static inline void *__efidir_calloc(int n, int s, char *source_file, int source_line){
  void *res=NULL;
  //__efidir_assert_message((n>0) && (s>0), "cannot calloc size 0 or 0 element", source_file, source_line);
  if((n<=0)||(s<=0)){
    fprintf(stderr,"WARNING: calloc size 0 or 0 element in %s:%i\n",source_file, source_line);
  }else{
    res=calloc(n,s);
    __efidir_assert_message(res!=NULL, "callocation failed", source_file, source_line);
  }
  return res;
}

/**
 * \ingroup allocation
 * \def efidir_free(void *p)
 * \brief like free, but set NULL to p and do not free p if it is already NULL
 *
 * \param p void* - an already allocated pointer
 * 
 */
#define efidir_free(p) (__efidir_free((void**)(&(p)), __FILE__, __LINE__))
static inline void  __efidir_free(void **p, char *source_file, int source_line){
  if (*p){
    free(*p);
    (*p)=NULL;
  }else{
    fprintf (stderr, "WARNING: %s:%u: free an already freed element\n", source_file, source_line);
  }
}

/**
 * \ingroup allocation
 * \def efidir_realloc(void *p,int s)
 * \brief like realloc, but die on error 
 *
 * \param p void* - an already allocated pointer
 * \param s int - size allocated in bytes
 * \return the value returned is a pointer to the allocated memory.
 */
#define efidir_realloc(p, s)(__efidir_realloc((void**)(&(p)), s, __FILE__, __LINE__))
static inline void *__efidir_realloc(void **p, int s, char *source_file, int source_line){
  void *res=NULL;
  if (s) {
    if (*p) {
      res=realloc((*p),s);
      __efidir_assert_message((res!=NULL), "re-allocation failed", source_file, source_line);
    } else {
      res=__efidir_malloc(s, source_file, source_line);
    }
  } else {
    if (*p) {
      __efidir_free(p, source_file, source_line);
    }
  }
  (*p)=res;
  return res;
}

/*
 * \ingroup allocation
 * \fn void *vector(int nc, int DataType);
 * \brief allocate memory for different type of vectors 
 *
 * \param nc int - size allocated in term of number of element
 * \param DataType corresponds to ImageDataType (for the moment only int, float complex8 are supported)
 * \return the value returned is a pointer to the allocated memory.
 *
 */
/*
 * \include ex_vector.c
 */
//void *vector(int nc, int DataType);

/*
 * \ingroup allocation
 * \fn void free_vector(void *in, int DataType);
 * \brief free memory for different type of vectors
 *
 * \param in void* - an already allocated vector pointer
 * \param DataType corresponds to ImageDataType (for the moment only int, float complex8 are supported)
 *
 */
/*
 * \include ex_vector.c
 */
//void free_vector(void *in, int DataType);

/*
 * \ingroup allocation
 * \fn void **matrix(int nl, int nc, int DataType);
 * \brief allocate memory for different type of matrices 
 *
 * \param nl int - size allocated in term of number of lines
 * \param nc int - size allocated in term of number of element per line
 * \param DataType corresponds to ImageDataType (for the moment only int, float complex8 are supported)
 * \return the value returned is a pointer to the allocated memory.
 *
 */
/*
 * \include ex_matrix.c
 */
//void **matrix(int nl, int nc, int DataType);

/*
 * \ingroup allocation
 * \fn void free_matrix(void **in, int nl, int DataType);
 * \brief free memory for different type of matrices 
 *
 * \param in void** - an already allocated matrix pointer
 * \param nl int - size allocated in term of number of lines
 * \param DataType corresponds to ImageDataType (for the moment only int, float complex8 are supported)
 *
 */
/*
 * \include ex_matrix.c
 */
//void free_matrix(void **in, int nl, int DataType);

#endif /* EFIDIR_ALLOCATION_H */
