/*****************************************************************************
 *
 * This example source code introduces a c library buffered I/O interface to
 * URL reads it supports fopen(), fread(), fgets(), feof(), fclose(),
 * rewind(). Supported functions have identical prototypes to their normal c
 * lib namesakes and are preceaded by url_ .
 *
 * Using this code you can replace your program's fopen() with url_fopen()
 * and fread() with url_fread() and it become possible to read remote streams
 * instead of (only) local files. Local files (ie those that can be directly
 * fopened) will drop back to using the underlying clib implementations
 *
 * See the main() function at the bottom that shows an app that retrives from a
 * specified url using fgets() and fread() and saves as two output files.
 *
 * Copyright (c) 2003 Simtec Electronics
 *
 * Re-implemented by Vincent Sanders <vince@kyllikki.org> with extensive
 * reference to original curl example code
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This example requires libcurl 7.9.7 or later.
 */ 

#ifndef EFIDIR_FILE_H
#define EFIDIR_FILE_H

#include <sys/types.h>
#ifndef _WIN32
# include <sys/time.h>
#endif
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#ifndef _WIN32
# include <unistd.h>
#endif

#ifdef _WIN32
# include <BaseTsd.h>
typedef SSIZE_T ssize_t;
typedef int mode_t;
#endif

#ifdef __CURL__
#include <curl/curl.h>
#include <curl/easy.h>
#endif //__CURL__

#include "efidir_stdint.h"
#include "efidir_string.h"
#include "efidir_file_txt.h"
#include "efidir_assert.h"

#ifdef __CURL__
enum fcurl_type_e { CFTYPE_NONE=0, CFTYPE_FILE=1, CFTYPE_CURL=2 };

enum fcurl_file_open_mthod {OTYPE_NONE=0,  OTYPE_OPEN=1, OTYPE_FOPEN=2 }; 

struct fcurl_data
{
  enum fcurl_type_e type;     /* type of handle */ 
  union {
    CURL *curl;
    FILE *file;
    int fd;
  } handle;                   /* handle */ 
  enum fcurl_file_open_mthod open_mthod; /* method used to open local file*/
  
  char *buffer;               /* buffer to store cached data*/ 
  int buffer_len;             /* currently allocated buffers length */ 
  int buffer_pos;             /* end of data in buffer*/ 
  int still_running;          /* Is background url fetch still in progress */ 
};
 
typedef struct fcurl_data URL_FILE;
 
typedef struct fileInfo_struct {
  char *filename;
  FILE *stream;

} fileInfo;

/* exported functions */ 
/* URL_FILE *url_open(const char *url, int flags, mode_t mode); */
/* URL_FILE *url_fopen(const char *url,const char *operation); */
/* int url_close(URL_FILE *file); */
/* int url_fclose(URL_FILE *file); */
/* int url_feof(URL_FILE *file); */
/* ssize_t url_read(URL_FILE *file, void *buf, size_t count); */
/* size_t url_fread(void *ptr, size_t size, size_t nmemb, URL_FILE *file); */
/* char * url_fgets(char *ptr, int size, URL_FILE *file); */
/* void url_rewind(URL_FILE *file); */
/* char *url_download1(const char *remoteFileName); */
char *url_download(const char *remoteFileName);

#endif //__CURL__


/**
 * \ingroup common
 * \defgroup file files and file names
 * \file efidir_file.h
 * \author Flavien Vernier
 *
 * \brief goodies to manage file and file names
 */

/**
 * \ingroup file
 * \fn char *get_next_file_name_from_file(FILE *fd, char *fileName);
 *
 * \brief return the next file name from "fileName"
 *
 * Read the next file name from "fileName".
 * As the file names into  "fileName" are relative to "fileName", the returned value is updated according to the path of "filename"
 * This function does not manage black line in "fileName" !
 *
 * \param fd : the file already open
 * \param fileName : the file name that contains a list of file names
 *
 * \return a string (char*) that contains the file name.
 */
char *get_next_file_name_from_file(FILE *fd, char *fileName);

/**
 * \ingroup file
 *
 * \brief Like classical open function.
 *
 * Like classical open function (man open). Function efidir_open manages 
 * remote file with curl.
 *
 * Let's note that remote file is downloaded and localy opened.
 * efidir_open stop the program if file does not exist
 *
 * \param url : the file already openname or url of file.
 * \param flags : see open.
 * \param mode : see open.
 *
 * \return The file descriptor (see open).
 */
int efidir_open(const char *url, int flags, mode_t mode);

/**
 * \ingroup file
 * \fn int efidir_open(const char *url, int flags, mode_t mode);
 *
 * \brief Like classical open function.
 *
 * Like classical open function (man open). Function efidir_open manages remote file with curl.
 * Let's note that remote file is downloaded and localy opened.
 *
 * \param url : the file already openname or url of file.
 * \param flags : see open.
 * \param mode : see open.
 *
 * \return The file descriptor (see open).
 */
int efidir_open_no_check(const char *url, int flags, mode_t mode);

/**
 * \ingroup file
 * \fn FILE* efidir_fopen(const char *url,const char *operation);
 *
 * \brief Like classical fopen function.
 *
 * Like classical fopen function (man fopen). Function efidir_fopen manages remote file with curl.
 * Let's note that remote file is downloaded and localy opened.
 * efidir_open stop the program if file does not exist
 *
 * \param url : the file already openname or url of file.
 * \param operation : see open.
 *
 * \return A pointer on FILE structure or NULL (see fopen).
 */
FILE* efidir_fopen_no_check(const char *url,const char *operation);
/**
 * \ingroup file
 * \fn FILE* efidir_fopen(const char *url,const char *operation);
 *
 * \brief Like classical fopen function.
 *
 * Like classical fopen function (man fopen). Function efidir_fopen manages remote file with curl.
 * Let's note that remote file is downloaded and localy opened.
 *
 * \param url : the file already openname or url of file.
 * \param operation : see open.
 *
 * \return A pointer on FILE structure or NULL (see fopen).
 */
FILE* efidir_fopen(const char *url,const char *operation);

/**
 * \ingroup file
 * \fn int efidir_close(int fd);
 *
 * \brief Like classical close function.
 *
 * Like classical close function (man close).
 *
 * \param fd : file descriptor
 *
 * \return see close.
 */
int efidir_close(int fd);

/**
 * \ingroup file
 * \fn int efidir_fclose(FILE *fp);
 *
 * \brief Like classical fclose function.
 *
 * Like classical fclose function (man fclose).
 *
 * \param fp : pointer on FILE structure.
 *
 * \return see fclose.
 */
int efidir_fclose(FILE *fp);

/**
 * \ingroup file
 * \fn int efidir_lseek(int fd, int64_t offset, int whence);
 *
 * \brief Like classical lseek function.
 *
 * Like classical lseek function (man 2 lseek). If platform requiere specific
 * functions to seek position with 64 bits offets, efidir_lseek() will
 * use them.
 *
 * \param fd : file descriptor
 * \param offset : offset of the repositionement
 * \param whence : reference position on which to apply offset (see lseek(2))
 *
 * \return see lseek(2).
 */
int efidir_lseek(int fd, int64_t offset, int whence);

/**
 * \ingroup file
 * \fn int exists_localy(const char *name);
 *
 * \brief Check if a file exist.
 *
 * Check if a file exist given by file name "name" exists localy.
 *
 * \param name : file name
 *
 * \return true(1)/false(0).
 */
int exists_localy(const char *fname);
#endif //FILE_H
