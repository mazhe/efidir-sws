/*
 *    Copyright (c) 2008. The EFIDIR team. All right reserved.
 *
 *    This file is part of EFIDIR tools.
 *
 *    EFIDIR tool(s) is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    EFIDIR tool(s) is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public licence
 *    along with EFIDIR tools.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "efidir_system.h"

#if defined HAVE_HWLOC

#include <hwloc.h>

#endif

#if defined HAVE_HWLOC
int get_core_number() {
  hwloc_topology_t topology;
    unsigned int ncores;

  hwloc_topology_init(&topology);
  hwloc_topology_load(topology);
  ncores = hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_CORE);
  hwloc_topology_destroy(topology);

  return (int)ncores;
}

#else /* defined HAVE_HWLOC */

#ifndef _WIN32
# include <sys/param.h>
#endif
#if defined BSD && !defined __GNU__
# include <sys/types.h>
# include <sys/sysctl.h>
#endif

int get_core_number() {
  int nbr_core=1;
#if defined BSD && !defined __GNU__
  int mib[] = { CTL_HW, HW_NCPU };
  size_t len = sizeof(nbr_core);
  efidir_assert(sysctl(mib, 2, &nbr_core, &len, NULL, 0) == 0); 
#elif defined __linux__
  {
    FILE *desc=NULL;
    char string[512];
    desc = fopen("/proc/cpuinfo", "r");
    if(desc!=NULL){
      nbr_core=0;
      while(!feof(desc)){
        fscanf(desc,"%s",string);
        if(strcmp(string, "processor")==0){
          nbr_core+=1;
        }
      }
      fclose(desc);
      if(nbr_core==0){
        nbr_core=1;
      }
    }
  }
#else
  fprintf(stderr,
      "WARNING: "
        "Cannot determine number of schedulable process on this platform, "
        "get_core_number() will return 1\n");
  nbr_core = 1;
#endif  /* define __linux__ */
  return nbr_core;
} 

#endif /* defined HAVE_HWLOC */

#if defined HAVE_HWLOC && HWLOC_API_VERSION > 0x00010000
void log_hwloc_topology(char* prog_name){
  hwloc_topology_t topology;
  char *xmlbuffer=NULL;
  int err, buflen;  
  char *xml_file_name = NULL;
  FILE *xml_file=NULL;

  xml_file_name = new_string(prog_name);
  string_cat(xml_file_name,"_hwloc.log");

  hwloc_topology_init(&topology);
  hwloc_topology_load(topology);

  err = hwloc_topology_export_xmlbuffer(topology, &xmlbuffer, &buflen); 

  xml_file = efidir_fopen(xml_file_name, "w");
  fprintf(xml_file, "%s", xmlbuffer);
  efidir_fclose(xml_file);

  hwloc_free_xmlbuffer(topology, xmlbuffer);
  hwloc_topology_destroy(topology);
}
#else
void log_hwloc_topology(char* prog_name){
fprintf(stderr,
    "WARNING: "
      "EFIDIR built without hwloc support, log_hwloc_topology cannont log "
      "info.\n");
}
#endif /* defined HAVE_HWLOC */
