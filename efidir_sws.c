/*
 *    Copyright (c) 2013. The EFIDIR team. All right reserved.
 *
 *    This file is part of EFIDIR tools.
 *
 *    EFIDIR tool(s) is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    EFIDIR tool(s) is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public licence
 *    along with EFIDIR tools.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * author: Matthieu Volat / ISTerre
 * 
 * EFIDIR processing workflow generator
 */

/* Useful links:
 * * IEEE 1003.1 reference specification for makefile syntax and make
 *   command-line arguments :
 *   http://pubs.opengroup.org/onlinepubs/009695399/utilities/make.html (2004)
 *   http://pubs.opengroup.org/onlinepubs/9699919799/utilities/make.html (2013)
 * 
 * Note:
 *   Obviously, the Posix spec is not enough to meet the requisite of 
 *   practical and efficient workflows. Therefore, we will take the following as
 *   given:
 *  * Command-line options should include -j <number_of_processus>
 *  * make(1) syntax should support the .PHONY attribute
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/wait.h>
#include <signal.h>
#include <libgen.h>

#include "efidir_allocation.h"
#include "efidir_file.h"
#include "efidir_string.h"
#include "efidir_system.h"

#include "efidir_sws.h"

struct struct_target {
  char **commands;
  size_t ncommands;
  char **inputs;
  size_t ninputs;
  char **outputs;
  size_t noutputs;
  target_properties properties;
};

struct struct_workflow {
  const char *name;
  Target *targets;
  size_t ntargets;
};


#ifdef HAVE_MPI
pid_t dispatchd;
static int get_dispatcher_poolsize()
{
	struct sockaddr_un addr;
	int sock;
	int poolsize;

    /* Create, bind, connect socket to send task */
    if ((sock = socket(PF_LOCAL, SOCK_STREAM, 0)) == -1) {
        perror("cannot create socket");
        exit(EXIT_FAILURE);
    }
    memset(&addr, 0, sizeof(addr));
    addr.sun_family = AF_UNIX;
    if (getenv("EFIDIR_DISPATCHD_SOCKET") == NULL) {
        snprintf(addr.sun_path,
                sizeof(addr.sun_path),
                "/tmp/tada-%zd.sock", getuid());
    }
    else {
        strncpy(addr.sun_path,
                getenv("EFIDIR_DISPATCHD_SOCKET"),
                sizeof(addr.sun_path)-1);
        addr.sun_path[sizeof(addr.sun_path)-1] = '\0';
    }
    if (connect(sock, (struct sockaddr *)&addr, sizeof(addr)) != 0) {
        perror("cannot connect socket");
        exit(EXIT_FAILURE);
    }
	/* Request worker pool size */
	send(sock, "EFIDIR_poolsize", strlen("EFIDIR_poolsize")+1, 0);
    recv(sock, &poolsize, sizeof(int), 0);
	close(sock);

	return poolsize-1;
}
#endif

static size_t _njobs = 0;


static int get_swslevel(void) {
  const char *s;
  s = getenv("EFIDIR_SWS_LEVEL");
  return (s == NULL) ? 0 : atoi(s);
}

size_t set_njobs(size_t njobs) {
#ifdef HAVE_MPI
  _njobs = njobs;
#else
  _njobs = (njobs == 0) ? get_core_number() : njobs;
#endif
  return _njobs;
}

size_t get_njobs(void) {
  return _njobs;
}

Target target_create() {
  Target t;

  t = efidir_calloc(1, sizeof(struct struct_target));

  return t;
}

void target_destroy(Target target) {
  for (int i = 0; i < target->ncommands; i++) {
    efidir_free(target->commands[i]);
  }
  for (int i = 0; i < target->ninputs; i++) {
    efidir_free(target->inputs[i]);
  }
  for (int i = 0; i < target->noutputs; i++) {
    efidir_free(target->outputs[i]);
  }
  efidir_free(target);
}

void target_addcommand(Target target, const char *command) {
  target->commands = (target->ncommands == 0)
    ? efidir_calloc(1, sizeof(char *))
    : efidir_realloc(target->commands, (target->ncommands+1)*sizeof(char *));
  
  efidir_assert(
      (target->commands[target->ncommands] = strdup(command)) != NULL);
  target->ncommands++;
}

void target_addworkflow(Target target, const char *command) {
  char *command2;
  command2 = new_string("#");
  command2 = string_cat(command2, command);
  target_addcommand(target, command2);
  efidir_free(command2);
}

void target_addinput(Target target, const char *input) {
  for (size_t i = 0; i < target->noutputs; i++) {
    efidir_assert(strcmp(input, target->outputs[i]) != 0);
  }

  target->inputs = (target->ninputs == 0)
    ? efidir_calloc(1, sizeof(char *))
    : efidir_realloc(target->inputs, (target->ninputs+1)*sizeof(char *));

  efidir_assert(
      (target->inputs[target->ninputs] = strdup(input)) != NULL);
  target->ninputs++;
}

void target_addoutput(Target target, const char *output) {
  for (size_t i = 0; i < target->ninputs; i++) {
    efidir_assert_message(
        strcmp(output, target->inputs[i]) != 0,
        "an output has the same path as an input");
  }

  target->outputs = (target->noutputs == 0)
    ? efidir_calloc(1, sizeof(char *))
    : efidir_realloc(target->outputs, (target->noutputs+1)*sizeof(char *));

  efidir_assert(
      (target->outputs[target->noutputs] = strdup(output)) != NULL);
  target->noutputs++;
}

void target_setproperties(Target target, target_properties properties) {
  target->properties = properties;
}


Workflow workflow_create(const char *name) {
  Workflow c;

  c = efidir_calloc(1, sizeof(struct struct_workflow));
  efidir_assert((c->name = strdup(name)) != NULL);

  return c;
}

void workflow_destroy(Workflow wf) {
  for (int i = 0; i < wf->ntargets; i++) {
    target_destroy(wf->targets[i]);
  }
  efidir_free(wf->name);
  efidir_free(wf);
}

void workflow_addtarget(Workflow wf, Target target) {
  wf->targets = (wf->ntargets == 0)
    ? efidir_calloc(1, sizeof(Target))
    : efidir_realloc(wf->targets, (wf->ntargets+1)*sizeof(Target));

  efidir_assert(
      (wf->targets[wf->ntargets] = target) != NULL);
  wf->ntargets++;
}

static void workflow_fwrite(Workflow wf, FILE *makefile, const char *path) {
  Target *targets = wf->targets;
  size_t ntargets = wf->ntargets;

  efidir_assert_message((ntargets > 0), "nothing to do, your workflow is emtpy");

  /* First, write a phony .MAIN target that require every ouputs */
  fputs(".PHONY: MAIN\nMAIN: ", makefile);
  for (size_t i = 0; i < ntargets; i++) {
    Target t = targets[i];
    for (size_t j = 0; j < t->noutputs; j++) {
      /* Check if this output is not only an intermediary result, ie is
       * another target's input list */
      bool intermediary = false;
      for (size_t k = 0; k < ntargets; k++) {
        Target t2 = targets[k];
        for (size_t l = 0; l < t2->ninputs; l++) {
          if (strcmp(t2->inputs[l], t->outputs[j]) == 0) {
            intermediary = true;
          }
        }
      }

      if (!intermediary) {
        fputs("\\\n\t", makefile);
        fputs(t->outputs[j], makefile);
        fputc(' ', makefile);
      }
    }
  }
  fputs("\n\n", makefile);

  /* Create each target rules */
  for (size_t i = 0; i < ntargets; i++) {
    Target t = targets[i];

    efidir_assert(t->noutputs > 0);

    /* Set the target properties */
    if (t->properties & TARGET_PHONY) {
      fputs(".PHONY: ", makefile);
      for (size_t j = 0; j < t->noutputs; j++) {
        fputs(t->outputs[j], makefile);
        fputc(' ', makefile);
      }
      fputc('\n', makefile);
    }

    /* Due to the way make(1) works, you cannot simply write a rule that 
     * have multiple outputs in one go (it will work with -j1, but break
     * with parallel jobs).
     * Here is a POSIX compliant commonly accepted workaround :
     *   output1: input1 input2 input3 ...
     *     cmd1
     *     ...
     *   output2 output3 ...: output1
     *     @if test -f $@; then touch -h $@; \
     *       else rm -f output1 && ${MAKE} output1; fi
     */

    /* First output rule */
    fputs(t->outputs[0], makefile);
    fputs(": ", makefile);
    for (size_t j = 0; j < t->ninputs; j++) {
      fputs(t->inputs[j], makefile);
      fputc(' ', makefile);
    }
    fputc('\n', makefile);
    /* Let us be nice and create ouput directories */
    fputs("\t@mkdir -p . ", makefile);
    for (size_t j = 0; j < t->noutputs; j++) {
      /* TODO: remove duplicates dirs */
      char *d = get_file_path(t->outputs[j]);
      fputs(d, makefile);
      fputc(' ', makefile);
      free(d);
    }
    fputc('\n', makefile);
    /* Then the commands, which are printed in green before being run */
    for (size_t j = 0; j < t->ncommands; j++) {
      fputs("\t-@echo \'", makefile);
      if (isatty(1)) {
        fputs("\x1B[32m", makefile);
      }
      for (int k = 0; k < get_swslevel()+1; k++) {
        fputc('+', makefile);
      }
      /* Printed command must be escaped */
      for (int k = 0; k < strlen(t->commands[j]); k++) {
        switch(t->commands[j][k]) {
        case '\'':
          fputs("\'\\\'\'", makefile);
          break;
        case '\n':
          fputs("\\n", makefile);
          break;
        default:
          fputc(t->commands[j][k], makefile);
          break;
        }
      }
      if (isatty(1)) {
        fputs("\x1B[0m", makefile);
      }
      fputs("\'\n", makefile);
      fputs("\t@", makefile);
      if (t->commands[j][0] == '#') {
        /* Command begining by # means subworkflow */
        fprintf(makefile, "${MAKE} -f /dev/null &>/dev/null; export EFIDIR_SWS_LEVEL=%d; ", get_swslevel()+1);
        fputs(&(t->commands[j][1]), makefile);
      }
      else {
        /* Regular command */
#ifdef HAVE_MPI
        fputs(LIBEXECDIR, makefile);
        fputs("/efidir_dispatch '", makefile);
        for (int k = 0; k < strlen(t->commands[j]); k++) {
          switch(t->commands[j][k]) {
          case '\'':
            fputs("\'\\\'\'", makefile);
            break;
          case '\n':
            fputs("\\n", makefile);
            break;
          default:
            fputc(t->commands[j][k], makefile);
            break;
          }
        }
        fputs("'", makefile);
#else
        for (int k = 0; k < strlen(t->commands[j]); k++) {
          switch(t->commands[j][k]) {
          case '\n':
            fputs("\\n", makefile);
            break;
          default:
            fputc(t->commands[j][k], makefile);
            break;
          }
        }
#endif
      }
      fputc('\n', makefile);
    }
    /* And manage multiple outputs secondary rules */
    if (t->noutputs == 1) {
      continue;
    }
    for (size_t j = 1; j < t->noutputs; j++) {
      fputs(t->outputs[j], makefile);
      fputc(' ', makefile);
    }
    fseek(makefile, -1, SEEK_CUR);
    fputs(": ", makefile);
    fputs(t->outputs[0], makefile);
    fputc('\n', makefile);
    fprintf(makefile,
        "\t@if test -f $@; then touch -h $@; else rm -f %s && ${MAKE} -f %s %s; fi\n",
        t->outputs[0], path, t->outputs[0]);
    fputc('\n', makefile);
  }
}

void workflow_save(Workflow wf, const char *path) {
  FILE *makefile;

  if (path == NULL) {
    path = wf->name;
  }
  efidir_assert((makefile = efidir_fopen(path, "w")) != NULL);
  workflow_fwrite(wf, makefile, path);
  efidir_assert(fclose(makefile) == 0);
}

static void sighandler(int signum, siginfo_t *siginfo, void *context) {
  /* Yeah, so certain implementation fork themselves and loose the parent, so
   * the PID we originaly got when we created the mpiexec subprocess may
   * not be valid anymore.
   * Luckily, we can retrieve the real PID of efidir_dispatchd rank 0 when
   * it signal us */
#ifdef HAVE_MPI
  dispatchd = siginfo->si_pid;
#endif
}

void workflow_run(Workflow wf, bool continue_on_error) {
  char makecmdbuf[768];
  char makefilepath[512];
  int status;

  /* Save workflow as temporary makefile */
#if defined HAVE_MKSTEMP
  strcpy(makefilepath, "/tmp/efidir.XXXXXX");
  efidir_assert(mkstemp(makefilepath) != 1);
#elif defined HAVE_MKTEMP
  strcpy(makefilepath, "/tmp/efidir.XXXXXX");
  efidir_assert(mktemp(makefilepath) != NULL);
#else
  efidir_assert(tmpnam(makefilepath) != NULL);
#endif
  workflow_save(wf, makefilepath);

#ifdef HAVE_MPI
  /* If toplevel workflow, start the dispatcher */
  if (get_swslevel() == 0) {
    char pidstr[12];
	char socketpath[256];
    snprintf(pidstr, sizeof(pidstr), "%d", getpid());
    snprintf(socketpath, sizeof(socketpath), "/tmp/tada-%d.sock", getpid());
    setenv("EFIDIR_SWS_LEVEL", "0", 1);
    setenv("EFIDIR_SWS_LEVEL0_PID", pidstr, 1);
    setenv("EFIDIR_DISPATCHD_SOCKET", socketpath, 1);

    if ((dispatchd = fork()) == 0) {
      /* Fork a process to become the dispatcher */
      char mpiexeccmdbuf[512];
      char *mpiexec;
      char *mpiexecflags;
      mpiexec = 
        getenv("EFIDIR_MPIEXEC") != NULL
          ? getenv("EFIDIR_MPIEXEC")
          : MPIEXEC; /* from config.h */
      mpiexecflags =
        getenv("EFIDIR_MPIEXEC_ARGS") != NULL
          ? getenv("EFIDIR_MPIEXEC_ARGS")
          : "";
      efidir_assert(snprintf(mpiexeccmdbuf,
            sizeof(mpiexeccmdbuf),
            "%s %s "LIBEXECDIR"/efidir_dispatchd",
            mpiexec, mpiexecflags) < sizeof(mpiexeccmdbuf)-1);
      execl("/bin/sh", "sh", "-c", mpiexeccmdbuf, NULL);
    }
    else {
      /* Main process will wait for dispatcher */
      struct sigaction sa;
      sigset_t mask;
      /* Remove one jobs that is used by the dispatcher */
      set_njobs(get_njobs()-1);
      /* Now wait for the dispatcher to give us signal to continue */
      /* - Register SIGUSR1 or its default handling will kill the process */
      sigemptyset(&sa.sa_mask);
      sa.sa_handler = NULL;
      sa.sa_flags = SA_SIGINFO|SA_RESTART;
      sa.sa_sigaction = &sighandler;
      sigaction(SIGUSR1, &sa, NULL);
      /* - Suspend the process until INT or USR1 is received */
      sigfillset(&mask);
      sigdelset(&mask, SIGINT);
      sigdelset(&mask, SIGUSR1);
      sigsuspend(&mask);
    }
  }
#else
  /* No dispatcher to get the parallelism capability, rely on system
   * function if no set number of jobs */
  if (_njobs == 0) {
    set_njobs(get_core_number());
  }
#endif

  /* Run make */
#ifdef HAVE_MPI
  efidir_assert(snprintf(makecmdbuf, 
        sizeof(makecmdbuf), 
        "make -j%d -f%s", 
        get_dispatcher_poolsize(), makefilepath) < sizeof(makecmdbuf)-1);
#else
  if (getenv("MAKEFLAGS") == NULL) {
    efidir_assert(snprintf(makecmdbuf, 
          sizeof(makecmdbuf), 
          "make %s -j%zu -f%s",
          continue_on_error ? "-k" : "", _njobs, makefilepath) < sizeof(makecmdbuf)-1);
  }
  else {
    efidir_assert(snprintf(makecmdbuf, 
          sizeof(makecmdbuf), 
          "make -f%s", 
          makefilepath) < sizeof(makecmdbuf)-1);
  }
#endif
  status = system(makecmdbuf);
  efidir_assert(status != -1);

  unlink(makefilepath);

#ifdef HAVE_MPI
  if (get_swslevel() == 0) {
    kill(dispatchd, SIGINT);
  }
#endif

  if (WEXITSTATUS(status) != 0) {
    fprintf(stderr, "Workflow %s execution failed\n", wf->name);
    exit(EXIT_FAILURE);
  }
}

void workflow_dryrun(Workflow wf, bool continue_no_inputs) {
  char makecmdbuf[768];
  char makefilepath[512];
  int status;

  if (continue_no_inputs) {
    /* We don't want the workflow to fail because of initial inputs absence,
     * so create a copy of wf where every input that is not another target's
     * output is removed so that workflow begin without input */
    Workflow wf2;
    wf2 = workflow_create("?");
    for (int i = 0; i < wf->ntargets; i++) {
      Target t, t2;
      t = wf->targets[i];
      t2 = target_create();
      for (int j = 0; j < t->ncommands; j++) {
        target_addcommand(t2, t->commands[j]);
      }
      for (int j = 0; j < t->ninputs; j++) {
        bool intermediary = false;
        for (size_t k = 0; k < wf->ntargets; k++) {
          Target t3 = wf->targets[k];
          for (size_t l = 0; l < t3->noutputs; l++) {
            if (strcmp(t3->outputs[l], t->inputs[j]) == 0) {
              intermediary = true;
            }
          }
        }

        if (intermediary) {
          target_addinput(t2, t->inputs[j]);
        }
      }
      for (int j = 0; j < t->noutputs; j++) {
        target_addoutput(t2, t->outputs[j]);
      }
      workflow_addtarget(wf2, t2);
    }
    wf = wf2;
  }

  /* Save workflow as temporary makefile */
#if defined HAVE_MKSTEMP
  strcpy(makefilepath, "/tmp/efidir.XXXXXX");
  efidir_assert(mkstemp(makefilepath) != 1);
#elif defined HAVE_MKTEMP
  strcpy(makefilepath, "/tmp/efidir.XXXXXX");
  efidir_assert(mktemp(makefilepath) != NULL);
#else
  efidir_assert(tmpnam(makefilepath) != NULL);
#endif
  workflow_save(wf, makefilepath);
  if (continue_no_inputs) {
    /* In that case, we operated on a temporary workflow struct, delete it */
    workflow_destroy(wf);
  }

  /* Run make */
  if (getenv("MAKEFLAGS") == NULL) {
    efidir_assert(snprintf(makecmdbuf, 
          sizeof(makecmdbuf), 
          "make -n -j1 -f%s", 
          makefilepath) < sizeof(makecmdbuf)-1);
  }
  else {
    efidir_assert(snprintf(makecmdbuf, 
          sizeof(makecmdbuf), 
          "make -n -f%s", 
          makefilepath) < sizeof(makecmdbuf)-1);
  }
  status = system(makecmdbuf);
  efidir_assert(status != -1);

  unlink(makefilepath);

  if (WEXITSTATUS(status) != 0) {
    fprintf(stderr, "Workflow %s execution failed\n", wf->name);
    exit(EXIT_FAILURE);
  }
}
