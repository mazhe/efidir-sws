#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int
main(int argc, char *argv[])
{
	struct sockaddr_un addr;
	int sock;

	char cwd[4096];
	char buf[4096+8192];
	int ret;

	if (argc < 2) {
		return EXIT_SUCCESS;
	}

	getcwd(cwd, sizeof(cwd));

	/* Create, bind, connect socket to send task */
	if ((sock = socket(PF_LOCAL, SOCK_STREAM, 0)) == -1) {
		perror("cannot create socket");
		exit(EXIT_FAILURE);
	}
	memset(&addr, 0, sizeof(addr));
	addr.sun_family = AF_UNIX;
	if (getenv("EFIDIR_DISPATCHD_SOCKET") == NULL) {
		snprintf(addr.sun_path,
				sizeof(addr.sun_path),
				"/tmp/tada-%zd.sock", getuid());
	}
	else {
		strncpy(addr.sun_path,
				getenv("EFIDIR_DISPATCHD_SOCKET"),
				sizeof(addr.sun_path)-1);
		addr.sun_path[sizeof(addr.sun_path)-1] = '\0';
	}
	if (connect(sock, (struct sockaddr *)&addr, sizeof(addr)) != 0) {
		perror("cannot connect socket");
		exit(EXIT_FAILURE);
	}

	/* Put cwd and cmd in a single buffer */
	strcpy(buf, cwd);
	strcpy(strchr(buf, '\0')+1, argv[1]);

	/* Send the request */
	send(sock, buf, strlen(cwd)+strlen(argv[1])+2, 0);

	/* Receive the return */
	recv(sock, &ret, sizeof(int), 0);

	close(sock);

	return ret;
}

