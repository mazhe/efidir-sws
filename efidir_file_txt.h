/*
 *    Copyright (c) 2008. The EFIDIR team. All right reserved.
 *
 *    This file is part of EFIDIR tools.
 *
 *    EFIDIR tool(s) is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    EFIDIR tool(s) is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public licence
 *    along with EFIDIR tools.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h> 
#include <ctype.h>

#ifdef _WIN32
# include <BaseTsd.h>
typedef SSIZE_T ssize_t;
#endif

#include "efidir_allocation.h"
#include "efidir_string.h"


#ifndef EFIDIR_FILE_TXT_H
#define EFIDIR_FILE_TXT_H
/**
 * \ingroup goodies
 * \defgroup textfile goodies to easier the management of  text file
 * \file efidir_file_txt.h
 * \author Balti hayethem
 * 
 * \brief Definition of text file facilities.
 * 
 *
 * 
 */
/**
 * \ingroup textfile 
 * \fn int nb_lines (const char *path)
 * \brief calculate the number of lines in a text file.
 *
 * \param path is the string that represents the path of the text file .
 *
 *
 * \return the number of lines (int)
 */
int nb_lines (const char *path);
/**
 * \ingroup textfile 
 * \fn int nb_lines (char *path)
 * \brief calculate the number of lines that contains text in a text file.
 *
 * \param path is the string that represents the path of the text file .
 *
 *
 * \return the number of lines (int)
 */
int nb_text_lines (char *path);

/**
 * \ingroup textfile 
 * \fn unsigned long file_how_many_words (char *path)
 * \brief calculate the number of words in a text file .
 *
 * \param path is the string that represents the path of the text file .
 *
 *
 * \return the number of words (unsigned long)
 */
unsigned long file_how_many_words (char *path);

/**
 * \ingroup textfile 
 * \brief Like getline but without '\n' 
 *
 * Like getline but without '\n' (cf man getline) for no_GNU OS.
 *
 * \param lineptr the address of the buffer containing the read text. 
          lineptr is automatically (re-)allocated if it is not long enough.
 * \param n the size of buffer lineptr. It can be modified if lineptr is 
          (re-)allocated.
 * \param stream the reading stream.
 *
 * \return On success, return the number of characters read, including the 
 *         delimiter character, but not including the terminating null 
 *         character. This value can be used to handle embedded null 
 *         characters in the line read. Return -1 on failure to read a line
 *         (including end of file condition). 
 */
ssize_t efidir_getline(char **lineptr, size_t *n, FILE *stream);

/**
 * \ingroup textfile 
 * \fn void efidir_remove_line(int lineIndice, char* path);
 * \brief Remove a line of a text file
 *
 * Remove line lineIndice (from 0) of the given text file.
 *
 * \param lineIndice indice of the line (from 0).
 * \param path path to the text file.
 */
void efidir_remove_line(int lineIndice, char* path);

/**
 * \ingroup textfile 
 * \fn char** file2tabOfLine(char *path);
 * \brief Convert file in a tab of lines
 *
 * Convert file in a tab of lines.
 * Each line of the file is a cell of the tab
 *
 * \return An arrai of char*
 *
 * \param path to the file to convert.
 */
char** file2tabOfLine(char *path);
#endif /*EFIDIR_FILE_TXT_H*/
