#include <sys/types.h>
#include <sys/param.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <sys/un.h>
#include <assert.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <mpi.h>

int rank, size;

static void
sighandler(int signum)
{
	char sun_path[4096];

	/* Quitting the dispatcher */

	/* Close socket */
	if (getenv("EFIDIR_DISPATCHD_SOCKET") == NULL) {
		snprintf(sun_path, sizeof(sun_path), "/tmp/tada-%zd.sock", getuid());
	}
	else {
		strncpy(sun_path,
				getenv("EFIDIR_DISPATCHD_SOCKET"),
				sizeof(sun_path)-1);
		sun_path[sizeof(sun_path)-1] = '\0';
	}
	unlink(sun_path);

	/* Quit */
	MPI_Finalize();
	exit(0);
}

/* TODO: Make that private to the rank 0 process */
int *avail_stack;
int *avail_cur;
pthread_mutex_t avail_mutex;
pthread_cond_t avail_cond;
int *clients;

void *
master_command_main(void *arg)
{
	pid_t sws_pid;

	int servsock;
	struct sockaddr_un addr;

	MPI_Status st;

	char buf[4096+8192];
	char *wd, *cmd;
	ssize_t wdlen, cmdlen;

	pthread_mutex_t mutex;

	(void)arg;
	
	/* Prepare internal mutex (for cond) */
	if (pthread_mutex_init(&mutex, NULL) != 0) {
		perror("cannot create mutex");
		exit(EXIT_FAILURE);
	}

	/* Create, bind, listen socket to listen from requests */
	if ((servsock = socket(PF_LOCAL, SOCK_STREAM, 0)) == -1) {
		perror("cannot create socket");
		exit(EXIT_FAILURE);
	}
	memset(&addr, 0, sizeof(addr));
	addr.sun_family = AF_UNIX;
	if (getenv("EFIDIR_DISPATCHD_SOCKET") == NULL) {
		snprintf(addr.sun_path, 
				sizeof(addr.sun_path), 
				"/tmp/tada-%zd.sock", getuid());
	}
	else {
		strncpy(addr.sun_path,
				getenv("EFIDIR_DISPATCHD_SOCKET"),
				sizeof(addr.sun_path)-1);
		addr.sun_path[sizeof(addr.sun_path)-1] = '\0';
	}
	unlink(addr.sun_path);
	if (bind(servsock, (struct sockaddr *)&addr, sizeof(addr)) != 0) {
		perror("cannot bind socket");
		exit(EXIT_FAILURE);
	}
	if (listen(servsock, 15) != 0) {
		perror("cannot bind socket");
		exit(EXIT_FAILURE);
	}

	/* Signal the sws we are ready */
	if (getenv("EFIDIR_SWS_LEVEL0_PID") != NULL) {
		sws_pid = atoi(getenv("EFIDIR_SWS_LEVEL0_PID"));
		kill(sws_pid, SIGUSR1);
	}

	/* Command main loop */
	while (1) {
		int sock;
		socklen_t addrlen;
		int worker;

		/* Accept and receive command from client */
		sock = accept(servsock, (struct sockaddr *)&addr, &addrlen);
		recv(sock, buf, sizeof(buf), 0);
		if (strcmp(buf, "EFIDIR_poolsize") == 0) {
			/* Special command to get the number of workers */
			send(sock, &size, sizeof(int), 0);
			close(sock);
		}
		else {
			wd = buf;
			wdlen = strlen(wd);
			cmd = strchr(buf, '\0')+1;
			cmdlen = strlen(cmd);

			if (avail_cur < avail_stack) { /* Ooops, no more workers availables */
				pthread_mutex_lock(&mutex);
				pthread_cond_wait(&avail_cond, &mutex);
				pthread_mutex_unlock(&mutex);
			}

			/* Take a worker */
			pthread_mutex_lock(&avail_mutex);
			worker = *avail_cur;
			avail_cur--;
			pthread_mutex_unlock(&avail_mutex);

			/* Send task */
			clients[worker] = sock;
			MPI_Send(wd, wdlen+1, MPI_CHAR, worker, 0, MPI_COMM_WORLD);
			MPI_Send(cmd, cmdlen+1, MPI_CHAR, worker, 0, MPI_COMM_WORLD);
		}
	} 

	/* Cleanup */
	close(servsock);
	unlink(addr.sun_path);
	pthread_mutex_destroy(&mutex);

	pthread_exit(EXIT_SUCCESS);
}

void *
master_control_main(void *arg)
{
	MPI_Status st;
	int ret;

	(void)arg;

	/* Control main loop */
	while (1) {
		/* Receive msg from worker */
		MPI_Recv(&ret, 1, MPI_INT, MPI_ANY_SOURCE, 1, MPI_COMM_WORLD, &st);
		/* Pass result to client and close */
		send(clients[st.MPI_SOURCE], &ret, sizeof(int), 0);
		close(clients[st.MPI_SOURCE]);

		/* Return worker to pool */
		pthread_mutex_lock(&avail_mutex);
		avail_cur++;
		*avail_cur = st.MPI_SOURCE;
		pthread_mutex_unlock(&avail_mutex);
		if (avail_cur <= avail_stack) { /* We were out of workers, signal */
			pthread_cond_signal(&avail_cond);
		}
	}

	pthread_exit(EXIT_SUCCESS);
}

void
worker_main(void)
{
	MPI_Status st;
	char cwd[4096], wd[4096], buf[8192];
	int status = 0;
	int ret = 1;

	getcwd(cwd, sizeof(cwd));

	/* Slave main loop */
	while (1) {
		MPI_Recv(wd, sizeof(wd), MPI_CHAR, 0, 0, MPI_COMM_WORLD, &st);
		MPI_Recv(buf, sizeof(buf), MPI_CHAR, 0, 0, MPI_COMM_WORLD, &st);
		if (strlen(wd) > 0 && chdir(wd) != 0) {
			perror("cannot chdir to work dir");
			ret = 255;
			MPI_Send(&ret, 1, MPI_INT, 0, 1, MPI_COMM_WORLD);
			continue;
		}
		status = system(buf);
		if (strlen(wd) > 0 && chdir(cwd) != 0) {
			perror("cannot chdir back from work dir");
			ret = 255;
			MPI_Send(&ret, 1, MPI_INT, 0, 1, MPI_COMM_WORLD);
			continue;
		}
		if (WIFEXITED(status)) {
			ret = WEXITSTATUS(status);
		}
		else if (WIFSIGNALED(status)) {
			ret = 1; /* Program signaled, that can't be good */
		}
		else {
			ret = 255; /* ??? happened */
		}
		MPI_Send(&ret, 1, MPI_INT, 0, 1, MPI_COMM_WORLD);
	}
}

int
main(int argc, char *argv[])
{
	int provided;

	MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
	assert(provided == MPI_THREAD_MULTIPLE);

	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	if (size < 2) {
		fputs("*************************\nTHE EFIDIR DISPATCHER MUST BE STARTED WITH AT LEAST TWO PROCESS\n*************************\n", stderr);
		exit(EXIT_FAILURE);
	}

	if (rank == 0) {
		struct sigaction sa;
		pthread_t command, control;

		sa.sa_handler = sighandler;
		sigemptyset(&sa.sa_mask);
		sa.sa_flags = SA_RESTART;
		sigaction(SIGINT, &sa, NULL);

		avail_stack = malloc((size-1)*sizeof(int));
		if (avail_stack == NULL) {
			abort();
		}
		for (size_t i = 1; i < size; i++) {
			avail_stack[i-1] = i;
		}
		avail_cur = &avail_stack[size - 2];
		if (pthread_mutex_init(&avail_mutex, NULL) != 0) {
			perror("cannot create mutex");
			exit(EXIT_FAILURE);
		}
		if (pthread_cond_init(&avail_cond, NULL) != 0) {
			perror("cannot create cond");
			exit(EXIT_FAILURE);
		}
		clients = malloc(size*sizeof(int));
		if (clients == NULL) {
			abort();
		}

		if (pthread_create(&command, NULL, master_command_main, NULL) != 0) {
			perror("cannot create thread");
			exit(EXIT_FAILURE);
		}
		if (pthread_create(&control, NULL, master_control_main, NULL) != 0) {
			perror("cannot create thread");
			exit(EXIT_FAILURE);
		}
		pthread_join(command, NULL);
		pthread_join(control, NULL);

		pthread_cond_destroy(&avail_cond);
		pthread_mutex_destroy(&avail_mutex);
		free(avail_stack);
	}
	else {
		worker_main();
	}

	MPI_Finalize();

	return EXIT_SUCCESS;
}
