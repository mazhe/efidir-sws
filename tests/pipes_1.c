/* Copyright (c) 2015. The EFIDIR team. All right reserved.
 *
 * This file is part of EFIDIR tools.
 *
 * EFIDIR tool(s) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EFIDIR tool(s) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with
 */

#include <assert.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "efidir_sws.h"

int save_stdout, devnull;

int init_suite(void)
{
  save_stdout = dup(STDOUT_FILENO);
  devnull = open("/dev/null", O_WRONLY);

  return 0; 
}

int clean_suite(void)
{
  close(devnull);

  return 0;
}

void test_pipes_1(void)
{
  /* Three tasks, 2 & 3 depends from 1, they all use output redirection */

  Workflow w;
  Target t1, t2, t3;
  FILE *res;

  w = workflow_create("test");
  assert(w != NULL);

  t1 = target_create();
  assert(t1 != NULL);
  target_addcommand(t1, "echo foo1 > foo1; echo foo2 > foo2 2>&1 | tee foo.log");
  target_addoutput(t1, "foo1");
  target_addoutput(t1, "foo2");
  workflow_addtarget(w, t1);

  t2 = target_create();
  assert(t2 != NULL);
  target_addcommand(t2, "cp foo1 bar1 2>&1 | tee -a foo.log");
  target_addinput(t2, "foo1");
  target_addoutput(t2, "bar1");
  workflow_addtarget(w, t2);

  t3 = target_create();
  assert(t3 != NULL);
  target_addcommand(t3, "cp foo2 bar2 2>&1 | tee -a foo.log");
  target_addinput(t3, "foo2");
  target_addoutput(t3, "bar2");
  workflow_addtarget(w, t3);

  /* Eat workflow output, it would mess the test report */
  dup2(devnull, STDOUT_FILENO);
  workflow_run(w, false);
  dup2(save_stdout, STDOUT_FILENO);

  workflow_destroy(w);

  res = fopen("foo1", "r");
  assert(res != NULL);
  fclose(res);
  unlink("foo1");

  res = fopen("foo2", "r");
  assert(res != NULL);
  fclose(res);
  unlink("foo2");

  res = fopen("bar1", "r");
  assert(res != NULL);
  fclose(res);
  unlink("bar1");

  res = fopen("bar2", "r");
  assert(res != NULL);
  fclose(res);
  unlink("bar2");

  res = fopen("foo.log", "r");
  assert(res != NULL);
  fclose(res);
  unlink("foo.log");
}

int main()
{
  test_pipes_1();
  return 0;
}
