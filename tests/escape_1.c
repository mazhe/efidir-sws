/* Copyright (c) 2015. The EFIDIR team. All right reserved.
 *
 * This file is part of EFIDIR tools.
 *
 * EFIDIR tool(s) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EFIDIR tool(s) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with
 */

#include <assert.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "efidir_sws.h"

int save_stdout, devnull;

int init_suite(void)
{
  save_stdout = dup(STDOUT_FILENO);
  devnull = open("/dev/null", O_WRONLY);

  return 0; 
}

int clean_suite(void)
{
  close(devnull);

  return 0;
}

void test_escape_1(void)
{
  /* One task */

  Workflow w;
  Target t;
  FILE *res;

  w = workflow_create("test");
  assert(w != NULL);

  t = target_create();
  assert(t != NULL);
  target_addcommand(t, "printf '\nfoo\n' > foo");
  target_addoutput(t, "foo");
  workflow_addtarget(w, t);

  /* Eat workflow output, it would mess the test report */
  dup2(devnull, STDOUT_FILENO);
  workflow_save(w, "foo");
  workflow_run(w, false);
  dup2(save_stdout, STDOUT_FILENO);

  workflow_destroy(w);

  res = fopen("foo", "r");
  assert(res != NULL);
  fclose(res);
  unlink("foo");
}

int main(void) {
  test_escape_1();
  return 0;
}
