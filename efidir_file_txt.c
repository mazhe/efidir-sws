/*
 *    Copyright (c) 2008. The EFIDIR team. All right reserved.
 *
 *    This file is part of EFIDIR tools.
 *
 *    EFIDIR tool(s) is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    EFIDIR tool(s) is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public licence
 *    along with EFIDIR tools.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "efidir_file_txt.h"

int nb_lines (const char *path){
  int n = 0;
  char * line=NULL;
  size_t lineSize;
  ssize_t readlen;
  FILE *fp;
  
  fp = fopen(path,"r");

  efidir_assert_fmessage(fp != NULL,"ERROR, can not open the list file: %s\n", path);

  while((readlen = efidir_getline(&line, &lineSize, fp)) != -1){
    n++;
  }
  
  fclose(fp);
  return n;
}

int nb_text_lines (char *path){
  int n=0/*, c*/;
  char * line=NULL;
  size_t lineSize;
  ssize_t read;
  FILE *fp;
  
  fp = fopen(path,"r");

  efidir_assert_fmessage(fp != NULL,"ERROR, can not open the list file: %s\n", path);

  while((read = efidir_getline(&line, &lineSize, fp)) != -1){
    trim(line);
    if(strlen(line) != 0){
      n++;
    }
  }

  fclose(fp);
  return n;

  
}

unsigned long file_how_many_words (char *path){
   
  long   cnt_w  = 0;
  
  FILE *p_file;
	
  
  p_file = fopen(path,"r");
  efidir_assert_fmessage(p_file != NULL,"ERROR, can not open the list file: %s\n", path);

  if (p_file != NULL){
    int c = 0;
    int mark = 0;
    
    
    while ((c = fgetc (p_file)) != EOF){
      if (! isspace (c) && mark == 0){
	cnt_w++;
	mark = 1;
      }else if (isspace (c)){
	mark = 0;
      }
    }  
    
    fclose (p_file);
  }
  return cnt_w;
}


ssize_t __efidir_getline(char **__lineptr, size_t *__n, FILE *__stream){

  size_t base_size = 256;
  size_t count=0;
  char c;
 
  if (((*__lineptr) == NULL) || ((*__n)<=0)){
    (*__n) = base_size;
    (*__lineptr) = (char*)efidir_malloc((*__n) * sizeof(char));
  }

  while(1) {	
    c = getc(__stream);
    if ((c == EOF)&&(count==0)) return -1;
    if ((c == EOF)&&(count!=0)) break;
    (*__lineptr)[count]=c;
    count++;
    if(count >= (*__n)-1){
      (*__n)+=base_size;
      (*__lineptr) = (char*)efidir_realloc((*__lineptr), (*__n) * sizeof(char));
    }
    if (c == '\n') break;
  }
  (*__lineptr)[count]='\0';

  return count;
}

ssize_t efidir_getline (char **__lineptr, size_t *__n, FILE *__stream){
  ssize_t count;
  count = __efidir_getline(__lineptr, __n, __stream);
  if (count != -1 && (*__lineptr)[count-1] == '\n') {
    /* Remove line return */
	/* Warning: what if the file as \r\n EOLs? */
    (*__lineptr)[count-1] = '\0';
  }
  return count;
}

void efidir_remove_line(int lineIndice, char* path){ 
  
  
  FILE *in_file = NULL, *out_file = NULL;
  char *output_file_name = strdup(path);
  char *line = NULL;
  size_t line_size;
  int count = 0;

  string_cat(output_file_name, "_tmp");

  in_file = fopen(path,"r");
  efidir_assert_fmessage(in_file != NULL,"ERROR, can not open the list file: %s\n", path);
  out_file =  fopen(output_file_name,"w");
  efidir_assert_fmessage(output_file_name != NULL,"ERROR, can not open the list file: %s\n", output_file_name);
  
  while(efidir_getline(&line, &line_size, in_file) != -1){
    if(count != lineIndice){
      fprintf(out_file, "%s\n", line);
    }
    count++;
  }
  fclose(in_file);
  fclose(out_file);

  rename(output_file_name, path);
}

char** file2tabOfLine(char *path){
  FILE *in_file = NULL;
  char *line = NULL;
  size_t line_size;
  int count = 0;

  char **result = efidir_calloc(nb_lines(path), sizeof(char*));

  in_file = fopen(path,"r");
  efidir_assert_fmessage(in_file != NULL,"ERROR, can not open the list file: %s\n", path);

  while(efidir_getline(&line, &line_size, in_file) != -1){
    result[count] = strdup(line);
    count++;
  }
  fclose(in_file);
  return result;
}
