/* Copyright (c) 2008. The EFIDIR team. All right reserved.
 *
 * This file is part of EFIDIR tools.
 *
 * EFIDIR tool(s) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EFIDIR tool(s) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EFIDIR tools.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \ingroup goodies
 * \defgroup string goodies to easier manage string
 * \file efidir_string.h
 * \author Flavien Vernier
 * 
 * \brief Definition of string facilities.
 * 
 * 
 * The string functions defined in EFIDIR allow to manage strings (char*) without manage there sizes.
 * Each function of string modul auto-manage the size of the strings.
 * The alocated size of a string is always equal to the number of character + 1 ('\\0').
 *
 * Exemple:
 *
 * \verbatim
  char *toto = new_string(NULL); //strlen(toto) gives 0, only one char is allocated.
  char *tutu = new_string("blabla"); //strlen(tutu) gives 6, exactly 7 char are allocated.
  string_cpy(toto, tutu); //toto is automatically re-allocated and has the same size of tutu.
  string_cat(toto, tutu); //toto is automatically re-allocated ...
  ...
 \endverbatim
 */

#ifndef EFIDIR_STRING_H
#define EFIDIR_STRING_H

#include <stdio.h>
#include <string.h>
#include <stddef.h>
#include <stdlib.h>
#include <limits.h>
#include <errno.h>
#include <math.h>
#include <ctype.h>

#include "efidir_allocation.h"
#include "efidir_boolean.h"


/**
 * \ingroup string
 * \fn new_string (const char *s)
 * \brief Create new string (char*) from s.
 *
 * \param s char* - A string.
 *
 * Create new string (char*) from s. 
 * If s is not NULL, string is duplicate into the new string 
 * otherwise, a new empty string is created.
 *
 * \return A new string contains the value of s
 *
 */
char* new_string(const char *s);

/**
 * \ingroup string
 * \def string_cpy (char* s1, char* s2)
 * \brief Copy s2 into s1.
 *
 * \param s1 char* - A string.
 * \param s2 char* - A string.
 *
 * Copy s2 into s1. 
 * If s1 is not long enough s1 is reallocated.
 * If s1 is NULL s1 is allocated.
 * If s2 is NULL s1 will be empty string.
 *
 * \return the value returned is the strcpy return (see man strcpy)
 *
 */
#define string_cpy(s1, s2)(__string_cpy(&(s1), s2))
void __string_cpy(char** string1, const char* string2);

/**
 * \ingroup string
 * \def string_cat(char* s1, char* s2)
 * \brief concat s2 at the end of s1.
 *
 * Concat s2 at the end of s1. s1 is always re-allocated
 *
 * \param s1 char* - A string.
 * \param s2 char* - A string.
 *
 * \return s1
 */
#define string_cat(s1, s2)(__string_cat(&(s1), s2))
char * __string_cat(char **s1, const char *s2);
/**
 * \ingroup string
 * \def string_ncat(char* s1, char* s2, int n)
 * \brief concat s2 at the end of s1.
 *
 * \param s1 char* - A string.
 * \param s2 char* - A string.
 * \param n int - number of charactere of s2 copied at the end of s1 
 *
 * Concat s2 at the end of s1. s1 is always re-allocated
 *
 */
#define string_ncat(s1, s2, n)(__string_cat(&(s1), s2, n))
void __string_ncat(char **s1, const char *s2, int n);

/**
 * \ingroup string
 * \fn char** string_split(char* s, char c)
 * \brief concat s2 at the end of s1.
 *
 * \param s char* - A string.
 * \param c char  - A char.
 *
 * Split the string s into a string array using delimiter c.
 * The string array is terminated by NULL.
 *
 * \return a string array terminated by NULL.
 *
 */
char **string_split(char* s, char c);


/**
 * \ingroup string
 * \fn char* string_to_lowercase(char *string);
 * \brief concat s2 at the end of s1translate into lower case.
 *
 * \param string char* - A string.
  *
 * Copy the parameter into a new string in lower case.
 *
 * \return a new string array that is the lower casse of parameter.
 *
 */
char* string_to_lowercase(char *string);


/**
 * \ingroup string
 * \def int_to_string (char *s, int i)
 * \brief Convert an integer to string
 *
 * \param s char* - The string that will contain the conversion.
 * \param i int - Value to convert.
 *
 * Convert a integer to string. If the string s is NULL or too short (strlen(s)<12), it is automaticaly (re)allocated (12 characters ) by the fuction.
 * Be careful, if the string is already allocated it must be "\0" terminated.
 *
 * \return the value returned is the sprintf return (see man sprintf)
 */
#define int_to_string(s, i)(__int_to_string(&(s), i, __FILE__, __LINE__))
static inline int __int_to_string(char **s, int i, char *source_file, int source_line){
  if(((*s)==NULL) || (strlen(*s)<11) ){ //10 digits + sign + '\0'
    __efidir_realloc((void**)s, 12, source_file, source_line);
  }
  fflush(stdout);
  return sprintf((*s),"%d",i);
}
/**
 * \ingroup string
 * \def long_to_string (char *s, long l)
 * \brief Convert an integer to string.
 *
 * \param s char* - The string that will contain the conversion.
 * \param l long - Value to convert.
 *
 * Convert a long integer to string. If the string s is NULL or too short (strlen(s)<20), it is automaticaly (re)allocated (21 characters ) by the fuction.
 * Be careful, if the string is already allocated it must be "\0" terminated.
 *
 * \return the value returned is the sprintf return (see man sprintf)
 */
#define long_to_string(s, l)(__long_to_string(&(s), l, __FILE__, __LINE__))
static inline long __long_to_string(char **s, long int l, char *source_file, int source_line){
  if(((*s)==NULL) || (strlen(*s)<20) ){ //19 digits + sign + '\0'
    __efidir_realloc((void**)s, 21, source_file, source_line);
  }
  return sprintf((*s),"%li",l);
}
/**
 * \ingroup string
 * \def double_to_string (char *s, int d)
 * \brief Convert a double to string.
 *
 * \param s char* - The string that will contain the conversion.
 * \param d int - Value to convert.
 *
 * Convert a double to string. If the string s if NULL or too short (strlen(s)<19), it is automaticaly (re)allocated (20 characters ) by the fuction.
 * Be careful, if the string is already allocated it must be "\0" terminated.
 *
 * \return the value returned is the sprintf return (see man sprintf)
 *
 */
#define double_to_string(s, d)(__double_to_string(&(s), d, __FILE__, __LINE__))
static inline int __double_to_string(char **s, double d, char *source_file, int source_line){
  if(((*s)==NULL) || (strlen(*s)<19) ){
    __efidir_realloc((void**)s, 20, source_file, source_line);
  }
  return sprintf((*s),"%e",d);
}
/**
 * \ingroup string
 * \def char_to_string (char *s, char c)
 * \brief Convert a char to string.
 *
 * \param s char* - The string that will contain the conversion.
 * \param c char  - Value to convert.
 *
 * Convert a char to string. If the string s if NULL or too short (strlen(s)<1), it is automaticaly (re-)allocated (2 characters ) by the fuction.
 * Be careful, if the string is already allocated it must be "\0" terminated.
 *
 * \return the value returned is the sprintf return (see man sprintf)
 *
 */
#define char_to_string(s, c)(__char_to_string(&(s), c, __FILE__, __LINE__))
static inline int __char_to_string(char **s, char c, char *source_file, int source_line){
  if(((*s)==NULL) || (strlen(*s)<1)){
    __efidir_realloc((void**)s, 2, source_file, source_line);
  }
  return sprintf((*s),"%c",c);
}


/**
 * \ingroup string
 *  \defgroup fileNames goodies to easier manage file names
 */
/**
 * \ingroup fileNames
 * \fn bool check_file_name_size(char *file_name)
 * \brief Check the length of a file name.
 *
 * \param file_name char* - A file name.
 *
 * Check the length of a file name.
 * Return true if the length is between 1 and 255 character.
 * TODO separate each part of path.
 * 
 *
 * \return True if the file name length is valid.
 *
 */
bool check_file_name_size(char *file_name);

/**
 * \ingroup fileNames
 * \fn char *get_file_name(char *file_name)
 * \brief Give the file name.
 *
 * \param file_name char* - A file name.
 *
 * Give the name of a file witout path and without extension.
 * Function get_file_name return a new alocated char* that must be freed by user.
 *
 * \return A new allocated char* that contains the file name witout path and without extension.
 *
 */
char *get_file_name(char *file_name);

/**
 * \ingroup fileNames
 * \fn char *get_file_extension(char *file_name)
 * \brief Give the file extension.
 *
 * \param file_name char* - A file name.
 *
 * Give the extension of a file. 
 * Exemple: with "/tmp/toto.tar.gz" get_file_extension return ".tar.gz".
 * Function get_file_extension return a new alocated char* that must be freed by user.
 *
 * \return A new allocated char* that contains the file extension.
 *
 */
char *get_file_extension(char *file_name);

/**
 * \ingroup fileNames
 * \fn char *get_file_last_extension(char *file_name);
 * \brief Give the last file extension.
 *
 * \param file_name char* - A file name.
 *
 * Give the last extension of a file.
 * Exemple: with "/tmp/toto.tar.gz" get_file_extension return ".gz".
 * Function get_file_last_extension return a new alocated char* that must be freed by user.
 *
 * \return A new allocated char* that contains the last file extension.
 *
 */
char *get_file_last_extension(char *file_name);

/**
 * \ingroup fileNames
 * \fn char *get_file_path(char *file_name);
 * \brief Give the path of the file.
 *
 * \param file_name char* - A file name.
 *
 * Give the path of a file.
 * Exemple: with "/tmp/toto.tar.gz" get_file_path return "/tmp".
 * Exemple: with "toto.tar.gz" get_file_path return ".".
 * Function get_file_extension return a new alocated char* that must be freed by user.
 *
 * \return A new allocated char* that contains the file path.
 *
 */
char *get_file_path(char *file_name);


/**
 * \ingroup fileNames
 * \fn char *add_file_ext(char *file_name, char *ext);
 * \brief Give the path of the file.
 *
 * \param file_name char* - A file name.
 * \param ext - extension
 *
 * Add the estension at the end of file_name.
 * Exemple: with "/tmp/toto" ".tar.gz" get_file_path return "/tmp/toto.tar.gz".
 * Warning, the dot between file_name and extension is not automaticaly added.
 * if the size of file_name is too short, file_name is reallocated.
 *
 * \return return a pointer in file_name.
 *
 */
char *add_file_ext(char *file_name, char *ext);

/**
 * \ingroup string
 * \fn void rtrim(char *str);
 * \brief remove space on the right.
 *
 * \param str char* - A string.
 *
 * Remove space on the right.
 *
 */
void rtrim(char *str);
/**
 * \ingroup string
 * \fn void ltrim(char *str);
 * \brief remove space on the left.
 *
 * \param str char* - A string.
 *
 * Remove space on the right.
 *
 */
void ltrim(char *str);
/**
 * \ingroup string
 * \fn void trim(char *str);
 * \brief remove space on the right an left.
 *
 * \param str char* - A string.
 *
 * Remove space on the right.
 *
 */
void trim(char *str);

/**
 * \ingroup string
 * \fn int string_to_int_or_die(char* string);
 * \brief Convert a string to int.
 *
 * \param string char* - A string that correspond to an int.
 *
 * Convert a string to int or die if the string does not represent an int.
 *
 * \return An integer.
 *
 */
int string_to_int_or_die(char* string);
/**
 * \ingroup string
 * \fn string_to_int(s, i);
 * \brief Convert a string to int.
 *
 * \param s char* - A string that correspond to an int.
 * \param i int   - The integer that received the value.
 *
 * Convert a string to int and return EXIT_SUCCESS if the conversion is OK.
 *
 * \return EXIT_FAILURE or EXIT_SUCCESS.
 *
 */
#define string_to_int(s, i) (__string_to_int(s, &(i)))
int __string_to_int(char* string, int* integer);

/**
 * \ingroup string
 * \fn long string_to_long_or_die(char* string);
 * \brief Convert a string to long.
 *
 * \param string char* - A string that correspond to a long.
 *
 * Convert a string to long int or die if the string does not represent a long int.
 *
 * \return An integer.
 *
 */
long string_to_long_or_die(char* string);
/**
 * \ingroup string
 * \fn string_to_long(s, l);
 * \brief Convert a string to long.
 *
 * \param s char* - A string that correspond to an int.
 * \param l long  - The long integer that received the value.
 *
 * Convert a string to long int and return EXIT_SUCCESS if the conversion is OK.
 *
 * \return EXIT_FAILURE or EXIT_SUCCESS.
 *
 */
#define string_to_long(s, l) (__string_to_long(s, &(l)))
int __string_to_long(char* string, long* long_integer);

/**
 * \ingroup string
 * \fn double string_to_double_or_die(char* string);
 * \brief Convert a string to double.
 *
 * \param string char* - A string that correspond to a double.
 *
 * Convert a string to double or die if the string does not represent a double.
 *
 * \return A double.
 *
 */
double string_to_double_or_die(char* string);
/**
 * \ingroup string
 * \fn string_to_double(s, d);
 * \brief Convert a string to double.
 *
 * \param s char*  - A string that correspond to a double.
 * \param d double - The double that received the value.
 *
 * Convert a string to double and return EXIT_SUCCESS if the conversion is OK.
 *
 * \return EXIT_FAILURE or EXIT_SUCCESS.
 *
 */
#define string_to_double(s, d) (__string_to_double(s, &(d)))
int __string_to_double(char* string, double* d);

/**
 * \ingroup string
 * \fn char string_to_char_or_die(char* string);
 * \brief Convert a string to char.
 *
 * \param string char* - A string that correspond to a char.
 *
 * Convert a string to char or die if the string does not represent a char.
 *
 * \return An double.
 *
 */
char string_to_char_or_die(char* string);
/**
 * \ingroup string
 * \fn string_to_char(s, c);
 * \brief Convert a string to char.
 *
 * \param s char* - A string that correspond to a char.
 * \param c char  - The char that received the value.
 *
 * Convert a string to char and return EXIT_SUCCESS if the conversion is OK.
 *
 * \return EXIT_FAILURE or EXIT_SUCCESS..
 *
 */
#define string_to_char(s, c) (__string_to_char(s, &(c)))
int __string_to_char(char* string, char* c);

/**
 * \ingroup string
 * \fn delete_space(s);
 * \brief Delete all space in string s.
 *
 * \param s char* - The string.
 *
 * Delete all space or tab into a given string return EXIT_SUCCESS.
 *
 * \return EXIT_FAILURE or EXIT_SUCCESS..
 *
 */
#define  delete_space(s) (__delete_space(&s, __FILE__, __LINE__))
int __delete_space(char** string, const char *source_file, int source_line);

/**
 * \ingroup string
 * \brief Test if a path is absolute.
 *
 * Test if the path passed as argument is absolute or not. The path does
 * not need to exist and will not be validated more than necessary.
 *
 * \param path  Path to test
 *
 * \return True if path is absolute, else false
 */
int path_is_absolute(const char *path);

/**
 * \ingroup string
 * \brief Concatenate path components
 *
 * Join multiple path components (path, path1, ..., pathN) into a single 
 * NULL-terminated string, using the system-appropriate separator between
 * each component.
 *
 * The path component list *MUST* be terminated by NULL.
 *
 * \param path  First component
 *
 * \return Newly allocated NULL-terminated path string
 */
char *path_join(const char *path, ... /*, NULL */);

#endif
