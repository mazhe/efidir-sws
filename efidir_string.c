/* Copyright (c) 2008. The EFIDIR team. All right reserved.
 *
 * This file is part of EFIDIR tools.
 *
 * EFIDIR tool(s) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EFIDIR tool(s) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EFIDIR tools.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "efidir_string.h"

char *new_string(const char *s){
  if(s!=NULL){
    return strdup(s);
  }else{
    char *sd = efidir_malloc(sizeof(char));
    sd[0] = '\0';
    return sd;
  }
}
void __string_cpy(char** string1, const char* string2){
  int size = 0;
  char *string = NULL;
  if(((*string1)!=NULL) && (string2!=NULL)){
    size = strlen(string2);
    if(strlen((*string1)) >= size){
      strcpy((*string1),string2);
    }else{
      free((*string1));
      (*string1) = strdup(string2);
    }
  }else if(((*string1)!=NULL) && (string2==NULL)){
    free((*string1));
    (*string1)=new_string(NULL);
  }else if(((*string1)==NULL)&& (string2!=NULL)){
    (*string1)=strdup(string2);    
  }else{
    (*string1)=new_string(NULL);
  }
}

char *__string_cat(char **string1, const char *string2) {
  int size = 0;
  char *string = NULL;

  efidir_assert(string1 != NULL);

  if (*string1 == NULL) {
    *string1 = strdup(string2);
    return *string1;
  }

  if (string2 == NULL) {
    return *string1;
  }

  size = strlen(*string1) + strlen(string2) + 1;
  string = efidir_malloc(size);
  strcpy(string, (*string1));
  strcat(string, string2);
  string[size-1] = '\0';

  efidir_free(*string1);
  *string1 = string;

  return string;
}

void __string_ncat(char** string1, const char* string2, int n){
  int size = 0;
  char *string = NULL;
  if(((*string1)!=NULL) && (string2!=NULL)){
    size = strlen(*string1)+n+1;
    string = (char*)efidir_malloc(size);
    strcpy(string, (*string1));
    strncpy(&(string[strlen(*string1)]), string2, n);
    string[size-1] = '\0';
    free(*string1);
    (*string1)=string;
  }else if((*string1)==NULL && string2!=NULL){
    (*string1)=strndup(string2, n);    
  }
}

char **string_split(char* s, char c){
  int i,j;
  char **string_array;
  if(s!=NULL){
    int nb_tokens=1;
    for(i=0; i<strlen(s);i++){
      if(s[i]==c){
        nb_tokens++;
      }
    }
    string_array = (char**)malloc((nb_tokens + 1) * sizeof(char*)); //+1 due to NULL terminaison
    
    j=0;
    string_array[j]=strdup(s);
    for(i=0; i<strlen(s);i++){
      if(s[i]==c){
        j++;
        string_array[0][i] = '\0';
        string_array[j] = &(string_array[0][i+1]);
      }
    }

    string_array[nb_tokens] = NULL;
  }else{
    string_array = (char**)malloc(sizeof(char*));
    string_array[0]=NULL;
  }
  return string_array;
}

bool check_file_name_size(char *file_name){
  int length = strlen(file_name);
  bool length_ok=false;
  if(length>1 && length<255){
    length_ok = true;
  }
  return length_ok;
}

char *get_file_name(char *file_name){
  char *begin=NULL, *end=NULL, *ret=NULL;

  begin = strrchr(file_name, '/');
  if(begin == NULL){
    begin = file_name;
  }else{
    begin++;
  }

  end = strchr(begin, '.');
  if(end == NULL){
    end = begin + strlen(file_name);
  }

  ret = strndup(begin, (size_t)(end - begin));

  return ret;  
}

char *get_file_extension(char *file_name){
  char *tmp, *ret=NULL;
  char *name=NULL;
  if(file_name !=NULL){
    name = strrchr(file_name, '/');
    if(name == NULL){
      name = file_name;
    }else{
      name++;
    }
    
    tmp = strchr(name, '.');
    if(tmp !=NULL){
      ret = strdup(tmp);
    }
    if(ret == NULL){
      ret = (char*)efidir_malloc(sizeof(char));
      ret[0] = '\0';
    }
  }
  return ret;
}

char *get_file_last_extension(char *file_name){
  char *tmp, *ret=NULL;
  if(file_name!=NULL){
    tmp = strrchr(file_name, '.');
    if(tmp !=NULL){
      ret = strdup(tmp);
    }else{
      ret = (char*)efidir_malloc(sizeof(char));
      ret[0] = '\0';
    }
  }
  return ret;
}
char *get_file_path(char *file_name){
  char *tmp, *ret=NULL;
  if(file_name!=NULL){
    tmp = strrchr(file_name, '/');
    if(tmp !=NULL){
      ret = strndup(file_name, tmp - file_name);
    }else{
      ret = (char*)efidir_malloc(2*sizeof(char));
      ret[0] = '.';
      ret[1] = '\0';
    }
  }
  return ret;
}

char *add_file_ext(char *name, char *ext)
{
  return string_cat(name, ext);
}

void rtrim(char *str)
{
  size_t n;
  n = strlen(str);
  while (n > 0 && isspace((unsigned char)str[n - 1])) {
    n--;
  }
  str[n] = '\0';
}

void ltrim(char *str)
{
  size_t n;
  n = 0;
  while (str[n] != '\0' && isspace((unsigned char)str[n])) {
    n++;
  }
  memmove(str, str + n, strlen(str) - n + 1);
}

void trim(char *str)
{
  rtrim(str);
  ltrim(str);
}

int __delete_space(char** string, const char *source_file, int source_line){
  int i,j;
  if((*string) == NULL){
    fprintf(stderr,
        "WARNING: %s, line %d: NULL string\n",
        source_file, source_line);
    return EXIT_FAILURE; 
  }

  for(i=0;i<strlen((*string));i++){
    if(((*string)[i]==' ') || ((*string)[i]=='\t')){
      for(j=i;j<strlen((*string));j++){
        (*string)[j]=(*string)[j+1];
      }
    }
  }
  return(EXIT_SUCCESS);
}

char *string_to_lowercase(char *string){
  char *lower = NULL;
  int i;

  string_cpy(lower, string);

  for(i = 0; i < strlen(lower); i++){
    lower[i] = tolower(lower[i]);
  }

  return lower;
}

int __string_to_int(char *string, int *integer){
  long val;
  double vad;
  char *endptr = NULL;
  char *copy_string=NULL;
  errno = 0;

  if((string == NULL) || (strlen(string)==0)){
    fprintf(stderr, "\"%s\" is not valid int\n", string);
    return EXIT_FAILURE; 
  }

  copy_string = strdup(string);

  delete_space(copy_string);

  if(copy_string[strlen(copy_string)-1]=='\n'){
    copy_string[strlen(copy_string)-1]='\0';
  }

  if(strlen(copy_string)==0){
    fprintf(stderr, "\"%s\" is not valid int\n", string);
    return EXIT_FAILURE; 
  }

  val = strtol(copy_string, &endptr, 10);
  if (
      (copy_string == NULL)
      || (errno == ERANGE && (val == LONG_MAX || val == LONG_MIN)) 
      || (errno != 0 && val == 0)
      || (val > INT_MAX) || (val < INT_MIN) 
      || (copy_string == endptr)
      || (*endptr != '\0')){
    fprintf(stderr, "\"%s\" is not valid int\n", string);
    free(copy_string);
    return EXIT_FAILURE;
  }
  free(copy_string);
  (*integer) = (int)val;
  return(EXIT_SUCCESS);  
}
int string_to_int_or_die(char* string){
  int val;

  efidir_assert(__string_to_int(string, &val)==EXIT_SUCCESS);

  return(val);  
}

int __string_to_long(char* string, long *long_integer){
  long val;
  double vad;
  char *endptr = NULL;
  char *copy_string=NULL;
  errno = 0;

  if((string == NULL) || (strlen(string)==0)){
    fprintf(stderr, "\"%s\" is not valid int\n", string);
    return EXIT_FAILURE; 
  }

  copy_string = strdup(string);

  delete_space(copy_string);

  if(copy_string[strlen(copy_string)-1]=='\n'){
    copy_string[strlen(copy_string)-1]='\0';
  }

  if(strlen(copy_string)==0){
    fprintf(stderr, "\"%s\" is not valid int\n", string);
    return EXIT_FAILURE; 
  }

  val = strtol(copy_string, &endptr, 10);

  if (
      (copy_string == NULL)
      || (errno == ERANGE && (val == LONG_MAX || val == LONG_MIN)) 
      || (errno != 0 && val == 0)
      || (copy_string == endptr)
      || (*endptr != '\0')){
    fprintf(stderr, "\"%s\" is not valid long\n", string);
    free(copy_string);
    return EXIT_FAILURE;
  }
  free(copy_string);
  (*long_integer) = val;
  return(EXIT_SUCCESS);  
}
long string_to_long_or_die(char* string){
  long val;

  efidir_assert(__string_to_long(string, &val)==EXIT_SUCCESS);

  return(val);  
}

int __string_to_double(char* string, double *d){
  long val;
  double vad;
  char *endptr = NULL;
  char *copy_string=NULL;
  errno = 0;

  if((string == NULL) || (strlen(string)==0)){
    fprintf(stderr, "\"%s\" is not valid double\n", string);
    return EXIT_FAILURE; 
  }

  copy_string = strdup(string);

  delete_space(copy_string);

  if(copy_string[strlen(copy_string)-1]=='\n'){
    copy_string[strlen(copy_string)-1]='\0';
  }

  if(strlen(copy_string)==0){
    fprintf(stderr, "\"%s\" is not valid double\n", string);
    return EXIT_FAILURE; 
  } 

  vad = strtod(copy_string, &endptr);

  if ((errno == ERANGE && (vad == HUGE_VAL || vad == -HUGE_VAL)) 
      || (errno != 0 && val == 0) 
      || (copy_string == endptr)
      || (*endptr != '\0')){
    fprintf(stderr, "\"%s\" is not valid double\n", string);
    free(copy_string);  
    return EXIT_FAILURE;
  }
  free(copy_string);  
  (*d) =  vad;
  return(EXIT_SUCCESS); 
}
double string_to_double_or_die(char* string){
  double vad;

  efidir_assert(__string_to_double(string, &vad)==EXIT_SUCCESS);

  return(vad);
}


int __string_to_char(char* string, char* c){
    if ((string==NULL) || (strlen(string) != 1)){
      fprintf(stderr,"\"%s\" is not valid char\n", string);
      return EXIT_FAILURE;
    }
    (*c) = string[0];
    return(EXIT_SUCCESS); 
}
char string_to_char_or_die(char* string){
  char vad;

  efidir_assert(__string_to_char(string, &vad)==EXIT_SUCCESS);

  return(vad);
}

char *string_to_string(char* string){

  char *string_clean;  
  int i,j;
  if(string == NULL){
    return NULL; 
  }

  string_clean = strdup(string);

  while((string_clean[0]==' ') || (string_clean[0]=='\t')){
    for(j=0;j<strlen(string_clean);j++){
      string_clean[j]=string_clean[j+1];
    }    
  }

  for(i=0;i<strlen(string_clean)-1;i++){
    if((string_clean[i]==' ') || (string_clean[i]=='\t')){
      while((string_clean[i+1]==' ') || (string_clean[i+1]=='\t')){
        if(string_clean[i+1]=='\t'){
          string_clean[i]='\t';
        }
        for(j=i+1;j<strlen(string_clean);j++){
          string_clean[j]=string_clean[j+1];
        }
      }
    }
  }

  if(string_clean[strlen(string_clean)-1] == '\n'){
    string_clean[strlen(string_clean)-1] = '\0';
  }
  if((string_clean[strlen(string_clean)-1]==' ') || (string_clean[strlen(string_clean)-1]=='\t')){
    string_clean[strlen(string_clean)-1] = '\0';
  }
  return string_clean;
}

int path_is_absolute(const char *path) {
#ifndef _WIN32
  return path[0] == '/';
#else
  return PathIsRelative(path) == 0;
#endif
}

char *path_join(const char *path, ... /*, NULL */) {
#ifndef _WIN32
  long maxpathsize = 4096; /* False, should be get through pathconf, but 
                            * path dependent... you see the problem? */
  char sep = '/';
#else
  long maxpathsize = 260; /* Really? */
  char sep = '\\';
#endif
  char *joined;
  va_list args;
  const char *patharg;

  /* Allocate new path to maximum possible size */
  joined = efidir_calloc(maxpathsize, sizeof(char));

  /* Copy initial component -- beware it does not already fill output */
  /* FIXME: trim trailing separators */
  strcpy(joined, path);
  efidir_assert(joined[maxpathsize-1] == '\0');

  /* Concatenate the rest of the components */
  va_start(args, path);
  while ((patharg = va_arg(args, const char *)) != NULL) {
    /* FIXME: trim trailing separators */
    strcat(joined, "/");
    strcat(joined, patharg);
    efidir_assert(joined[maxpathsize-1] == '\0');
  }
  va_end(args);

  return joined;
}
